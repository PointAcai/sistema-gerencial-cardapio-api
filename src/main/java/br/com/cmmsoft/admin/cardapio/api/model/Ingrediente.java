package br.com.cmmsoft.admin.cardapio.api.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.cmmsoft.admin.cardapio.api.model.converter.BooleanAtivoInativoConverter;

@Entity
@Table(name="T_INGREDIENTE")
public class Ingrediente {

	/*
	 * ATRIBUTOS
	 * */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codigo")
	private Long codigo;
	
	@Column(name = "descricao")
	private String descricao;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "T_PRODUTO_INGREDIENTE", 
			joinColumns = @JoinColumn(name = "CODIGO_INGREDIENTE"),
			inverseJoinColumns = @JoinColumn(name = "CODIGO_PRODUTO")
	)
	@JsonIgnore
    private Collection<Produto> produtos;
	
	@Column(name = "status")
	@Convert(converter = BooleanAtivoInativoConverter.class)
	private Boolean statusIngrediente;
	
	/*
	 * GETTERS E SETTERS
	 * */
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Collection<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(Collection<Produto> produtos) {
		this.produtos = produtos;
	}

	public Boolean getStatusIngrediente() {
		return statusIngrediente;
	}

	public void setStatusIngrediente(Boolean statusIngrediente) {
		this.statusIngrediente = statusIngrediente;
	}
	
	/*
	 * EQUALS E HASHCODE
	 * */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ingrediente other = (Ingrediente) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
}
