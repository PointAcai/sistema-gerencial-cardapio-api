/*
 * Classe Responsável pela Segurança da API Utilizando OAuth2
 * */
package br.com.cmmsoft.admin.cardapio.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.expression.OAuth2MethodSecurityExpressionHandler;

/* Anotação de Configuração da API, 
 * outra para Habilitar a Segurança,
 * outra pra Habilitar a Configuração do Servidor para OAuth2 e
 * outra para Habilitar a Segurança dos Métodos
 * */
@Profile("oauth-security") 
@Configuration
@EnableWebSecurity
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	/*
	 * Método Responsável pela Configuração.
	 * Ex: De onde eu vou Validar o Usuario e Senha Validado na API
	 * */
	@Autowired
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		/*
		 *  Autenticação será feita em Memória, passando o Usuário, Senha(Usado na Autenticação)
		 *  e uma Permissão(ROLE: Usado na Autorização)
		 * */ 
//		auth.inMemoryAuthentication()
//			.withUser("admin").password("admin").roles("ROLE");
		
		// Passa por Parâmetro os Detalhes do Usuário da Aplicação e suas Permissões
		// É Preciso Informar como Senha foi Encodada 
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	
	/*
	 * Método Responsável pela Autorização das Requisições.
	 * */
	@Override
	public void configure(HttpSecurity http) throws Exception {
		/*
		 * Define que Qualquer de Tipo de Requisição é necessário está Autenticado!
		 * Ou seja, é necessário que o Usuário e Senha esteja Validado em Qualquer Requisição
		 * A Propriedade .antMatchers("/perfis").permitAll() = Retira a Restrição de Autorização para ser Efetuada
		 * A Propriedade SessionCreationPolicy.STATELESS = Desabilita a Criação de Sessão no Servidor (Sem Estado)
		 * 
		 * RETIRADO A Propriedade .and().httpBasic() = Utiliza a Autenticação Básica
		 * */ 
		http.authorizeRequests()
			.antMatchers("/cardapio").permitAll()
			.and().authorizeRequests()
			.antMatchers("/parametros").permitAll()
			.and().authorizeRequests()
	        .anyRequest().authenticated()
			.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and().csrf().disable();
	}
	
	// Responsável por Deixar o Servidor Stateless, ou seja, não Mantém Estado Nenhum no Servidor
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.stateless(true);
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	// Método Responsável para Habilitar a Segurança dos Métodos utilizando o OAuth2
	@Bean
	public MethodSecurityExpressionHandler createExpressionHandler() {
		return new OAuth2MethodSecurityExpressionHandler();
	}
	
}
