package br.com.cmmsoft.admin.cardapio.api.service.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import br.com.cmmsoft.admin.cardapio.api.model.Produto;

public class OfertaDTO {

	/*
	 * ATRIBUTOS
	 * */
	
	private Long codigo;
	private Produto produto;
	private String descricao;
	private BigDecimal valorPromocional;
	private List<LocalDateTime> dataValidade;
	private Boolean statusOferta;
	
	/*
	 * GETTERS E SETTERS
	 * */

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValorPromocional() {
		return valorPromocional;
	}

	public void setValorPromocional(BigDecimal valorPromocional) {
		this.valorPromocional = valorPromocional;
	}

	public List<LocalDateTime> getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(List<LocalDateTime> dataValidade) {
		this.dataValidade = dataValidade;
	}

	public Boolean getStatusOferta() {
		return statusOferta;
	}

	public void setStatusOferta(Boolean statusOferta) {
		this.statusOferta = statusOferta;
	}
			
}
