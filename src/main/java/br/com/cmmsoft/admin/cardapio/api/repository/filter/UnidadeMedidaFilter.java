package br.com.cmmsoft.admin.cardapio.api.repository.filter;

public class UnidadeMedidaFilter {
	
	private String sigla;
	private String descricao;
	private Boolean status;
	
	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
	
}
