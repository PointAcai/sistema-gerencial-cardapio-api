package br.com.cmmsoft.admin.cardapio.api.resource;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.cmmsoft.admin.cardapio.api.event.RecursoCriadoEvent;
import br.com.cmmsoft.admin.cardapio.api.exceptionhandler.SisVendaExceptionHandler.Erro;
import br.com.cmmsoft.admin.cardapio.api.model.Perfil;
import br.com.cmmsoft.admin.cardapio.api.repository.PerfilRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.PerfilFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoPerfil;
import br.com.cmmsoft.admin.cardapio.api.service.PerfilService;
import br.com.cmmsoft.admin.cardapio.api.service.exception.PerfilInexistenteInativoException;

@RestController
@RequestMapping("/perfis")
public class PerfilResource {
	
	@Autowired
	private PerfilRepository perfilRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private PerfilService perfilService;
	
	@Autowired
	private MessageSource messageSource;
	
	// Método Pesquisar com Paginação Através dos Atributos Definidos para Pesquisa
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PERFIL') and #oauth2.hasScope('read')")
	public Page<Perfil> pesquisar(PerfilFilter perfilFilter, Pageable pageable) {
		return perfilRepository.filtrar(perfilFilter, pageable);
	}
	
	// Método Pesquisar de Resumo com Paginação
	@GetMapping(params = "resumo")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PERFIL') and #oauth2.hasScope('read')")
	public List<ResumoPerfil> resumir() {
		return perfilRepository.resumir();
	}
	
	// Método Pesquisar Através do Código
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PERFIL') and #oauth2.hasScope('read')")
	public ResponseEntity<Perfil> pesquisarUsuarioCodigo(@PathVariable Long codigo) {
		Perfil perfilSalvo = perfilRepository.findOne(codigo);
		return perfilSalvo != null ? ResponseEntity.ok(perfilSalvo) : ResponseEntity.notFound().build();
	}
	
	// Método Cadastrar
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_PERFIL') and #oauth2.hasScope('write')")
	public ResponseEntity<Perfil> cadastrar(@Valid @RequestBody Perfil perfil, HttpServletResponse response) {
		Perfil perfilSalvo = perfilService.cadastrar(perfil);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, perfilSalvo.getCodigo()));
		return ResponseEntity.status(HttpStatus.CREATED).body(perfilSalvo);
	}
	
	// Método Excluir
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_EXCLUIR_PERFIL') and #oauth2.hasScope('write')")
	public void excluir(@PathVariable Long codigo) {
		perfilRepository.delete(codigo);
	}
	
	// Método Alterar
	@PutMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_PERFIL') and #oauth2.hasScope('write')")
	public ResponseEntity<Perfil> alterar(@PathVariable Long codigo, @Valid @RequestBody Perfil perfil) {
		Perfil perfilSalvo = perfilService.alterar(codigo, perfil);
		return ResponseEntity.ok(perfilSalvo);
	}
	
	// Método Alterar Propriedade Status
	@PutMapping("/{codigo}/status")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_PERFIL') and #oauth2.hasScope('write')")
	public void alterarStatus(@PathVariable Long codigo, @RequestBody Boolean ativo) {
		perfilService.alterarStatus(codigo, ativo);
	}
	
	/*
	 * Método que Implementa a Exceção dessa Funcionalidade
	 * */
	public ResponseEntity<Object> handlePerfilInexistenteInativoException(PerfilInexistenteInativoException ex) {
		String mensagemUsuario = messageSource.getMessage("perfil.inexistente-inativo", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}
	
}
