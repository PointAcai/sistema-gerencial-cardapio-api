package br.com.cmmsoft.admin.cardapio.api.service.dto;

import java.math.BigDecimal;
import java.util.Collection;

import br.com.cmmsoft.admin.cardapio.api.model.Categoria;
import br.com.cmmsoft.admin.cardapio.api.model.Ingrediente;
import br.com.cmmsoft.admin.cardapio.api.model.UnidadeMedida;

public class ProdutoDTO {

	/*
	 * ATRIBUTOS
	 * */

	private Long codigo;
	private Long numIdentificador;
	private String descricao;
	private Long quantidadeMedida;
	private UnidadeMedida unidadeMedida;
	private Collection<Ingrediente> ingredientes;
	private Categoria categoria;
	private BigDecimal valor;
	private String exibirCardapio;
	private String imagem;
	private String urlImagem;
	private String exibirComo;
	private String tipoComplemento;
	
	/*
	 * GETTERS E SETTERS
	 * */
	
	public Long getNumIdentificador() {
		return numIdentificador;
	}
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public void setNumIdentificador(Long numIdentificador) {
		this.numIdentificador = numIdentificador;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public Long getQuantidadeMedida() {
		return quantidadeMedida;
	}
	
	public void setQuantidadeMedida(Long quantidadeMedida) {
		this.quantidadeMedida = quantidadeMedida;
	}
	
	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}
	
	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	
	public Collection<Ingrediente> getIngredientes() {
		return ingredientes;
	}
	
	public void setIngredientes(Collection<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}
	
	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public BigDecimal getValor() {
		return valor;
	}
	
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getExibirCardapio() {
		return exibirCardapio;
	}

	public void setExibirCardapio(String exibirCardapio) {
		this.exibirCardapio = exibirCardapio;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}

	public String getExibirComo() {
		return exibirComo;
	}

	public void setExibirComo(String exibirComo) {
		this.exibirComo = exibirComo;
	}

	public String getTipoComplemento() {
		return tipoComplemento;
	}

	public void setTipoComplemento(String tipoComplemento) {
		this.tipoComplemento = tipoComplemento;
	}
	
}
