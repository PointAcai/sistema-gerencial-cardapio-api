/*
 * Classe Responsável pela Autorização da API Utilizando OAuth2 Utizando o JWT para Encode do Token
 * 
 * Para Testar: Utilizar Postman, Solicitar uma Requisição POST para localhost:8080/oauth/token e Autorização BASIC 
 * Informando Login e Usuário do Cliente, nesse Caso, utiliza o Usuário e Senha do Cliente que vai usar a API, 
 * ou seja, usuário configurado no método que sobrescreve  configure(ClientDetailsServiceConfigurer clients).
 * Passar também no Corpo da Requisição em Body e Selecionar a Opção x-www-form-urlencoded e digitar em Key a palavra 'Client'
 * e no Value digitar 'angular', novamente, agora utilizando o Login e Senha da Aplicação 
 * em Key digitar a palavra 'username' e no Value digitar 'admin' depois em Key digitar a palavra 'password' e em Value digitar 'admin'.
 * Adicionar outra informação, digitando em Key a palavra 'grant_type' e em Value 'password'
 * com isso, é adicionado no Header um Content-Type.
 * 
 * Agora, nas Requisições, mudar o Authorization para "NO-AUTH", clicar na Aba Headers, em Key, digitar a Palavra Authorization
 * e em Value o Bearer 47605046-4e23-45bc-9383-37db3b0d838c.
 * */
package br.com.cmmsoft.admin.cardapio.api.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import br.com.cmmsoft.admin.cardapio.api.config.token.CustomTokenEnhancer;

@Profile("oauth-security")
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
	
	// Responsável por Gerenciar a Autenticação, ele consegue pegar o Usuário e Senha da Aplicação
	@Autowired
	private AuthenticationManager authenticationManager;
	
	/*
	 * Método Responsável por Autorizar o Cliente(Aplicação) a Acessar o AuthorizationServer
	 * Armazena o Cliente em Memória (Pode ser configurado em Banco de Dados)
	 * Propriedade .withClient("angular") = Quem será o Cliente que terá Acesso a Aplicação
	 * Propriedade .secret("@ngul@r0") = Chave de Acesso desse Cliente
	 * Propriedade .scopes("read", "write") = Permite Limitar o Acesso desse Cliente
	 * Propriedade .authorizedGrantTypes("password", "refresh_token") = Fluxo onde a Aplicação recebe o Usuário e Senha do Usuário 
	 * e Enviar pra pegar o Acess Token e o Refresh Token
	 * Propriedade .accessTokenValiditySeconds(1800); = Define quanto tempo o Token (1800/60 = 30 Min) estará Ativo e Válido
	 * Propriedade .refreshTokenValiditySeconds(3600*24); = Define o Tempo do Refresh Token pra Expirar de 1(Um) dia
	 * 
	 * Adicionado um Novo Cliente de Acesso a API: Ex = MOBILE, com Scopo de Leitura
	 * */
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory()
			.withClient("angular")
			.secret("@ngul@r0")
			.scopes("read", "write")
			.authorizedGrantTypes("password", "refresh_token")
			// .accessTokenValiditySeconds(1800)
			// .refreshTokenValiditySeconds(3600*24)
			
			.and()
			
			.withClient("mobile")
			.secret("m0b1l30")
			.scopes("read")
			.authorizedGrantTypes("password", "refresh_token");
			// .accessTokenValiditySeconds(1800)
			// .refreshTokenValiditySeconds(3600*24);
	}
	
	/*
	 * Propriedade .tokenStore(tokenStore()) = Método Responsável por Armazenar o Token
	 * Propriedade .accessTokenConverter = Conversor de Token
	 * Propriedade .authenticationManager(authenticationManager) = Valida se a Configuração está Toda Certa e Guarda o Token Liberado
	 * Propriedade .reuseRefreshTokens(false) = False: Significa que Sempre que eu pedir um Token ele vai setar um Novo Refresh Token
	 * Adiciona novos EndPoints de Acesso à Aplicação para Solicitar Autorização através de Token: Ex: localhost:8080/oauth/token
	 * */
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		
		// Configuração ou Melhoria para Colocar Nome do Usuário no Token
		TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
		tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), accessTokenConverter()));
		
		endpoints
			.tokenStore(tokenStore())
			.tokenEnhancer(tokenEnhancerChain)
			.reuseRefreshTokens(false)
			.authenticationManager(authenticationManager);
	}

	private TokenEnhancer tokenEnhancer() {
		return new CustomTokenEnhancer();
	}

	/*
	 * Método Responsável por Converter o Token utilizando JWT
	 * accessTokenConverter.setSigningKey("algaworks") = Senha Responsável por Validar o Token
	 * A Anotação do @Bean é pra quem precisar Utilizar do accessTokenConverter consegui chama-lo Através desse Método public
	 * */
	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter();
		accessTokenConverter.setSigningKey("algaworks");
		return accessTokenConverter;
	}

	/*
	 * Método Responsável por Armazenar o Token na Memória do JWT
	 * Pode ser Configurado para Armazenar no Banco de Dados
	 * */
	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}
	
}
