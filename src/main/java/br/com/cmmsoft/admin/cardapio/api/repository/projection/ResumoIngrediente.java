package br.com.cmmsoft.admin.cardapio.api.repository.projection;

public class ResumoIngrediente {
	
	private Long codigo;
	private String descricao;
	private Boolean statusIngrediente;
	
	// Construtor é Utilizado para Realizar a Consulta pela Query, respeitar a Ordem Definido no RespositoryImpl
	public ResumoIngrediente(Long codigo, String descricao, Boolean status) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.statusIngrediente = status;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getStatusIngrediente() {
		return statusIngrediente;
	}

	public void setStatusIngrediente(Boolean statusIngrediente) {
		this.statusIngrediente = statusIngrediente;
	}
	
}
