package br.com.cmmsoft.admin.cardapio.api.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.cmmsoft.admin.cardapio.api.model.converter.BooleanAtivoInativoConverter;

@Entity
@Table(name="T_USUARIO")
public class Usuario {
	
	/*
	 * ATRIBUTOS
	 * */
	
	// GenerationType.IDENTITY = MYSQL Responsável pelo Auto Increment
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	
	// Atribui a Validação NOT NULL para Esse Campo (Usar o @Valid no método de Cadastrar)
	@NotNull
	@Size(min = 3, max = 50)
	@Column(name = "nome")
	private String nome;
	
	@NotNull
	@Size(min = 3, max = 20)
	@Column(name = "login")
	private String login;
	
	@NotNull
	@Column(name = "senha")
	private String senha;
	
	// ManyToOne = Many (Esse Objeto 'Usuario' vai ter um Único Objeto 'Perfil')
	@ManyToOne
	@JoinColumn(name = "CODIGO_PERFIL")
	private Perfil perfil;
	
	@Column(name = "status")
	@Convert(converter = BooleanAtivoInativoConverter.class)
	private Boolean status;

	/*
	 * GETTERS E SETTERS
	 * */
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	/*
	 * EQUALS E HASHCODE
	 * */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
}
