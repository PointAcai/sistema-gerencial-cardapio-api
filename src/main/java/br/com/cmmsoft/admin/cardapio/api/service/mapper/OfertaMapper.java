package br.com.cmmsoft.admin.cardapio.api.service.mapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.Oferta;
import br.com.cmmsoft.admin.cardapio.api.service.dto.OfertaDTO;

@Service
public class OfertaMapper {

	public OfertaDTO ofertaToOfertaDTO(Oferta oferta) {
		OfertaDTO ofertaDTO = new OfertaDTO();
		ofertaDTO.setCodigo(oferta.getCodigo());
		List<LocalDateTime> validades = new ArrayList<>();
		validades.add(oferta.getDataValidadeInicio());
		validades.add(oferta.getDataValidadeFim());
		ofertaDTO.setDataValidade(validades);
		ofertaDTO.setProduto(oferta.getProduto());
		ofertaDTO.setStatusOferta(oferta.getStatusOferta());
		ofertaDTO.setDescricao(oferta.getDescricao());
		ofertaDTO.setValorPromocional(oferta.getValorPromocional());
        return ofertaDTO;
    }
	
	public List<OfertaDTO> ofertasToOfertaDTOs(List<Oferta> oferta) {
        return oferta.stream()
            .filter(Objects::nonNull)
            .map(this::ofertaToOfertaDTO)
            .collect(Collectors.toList());
    }
	
	public Oferta ofertaDTOToOferta(OfertaDTO ofertaDTO) {
		try {
	        if (ofertaDTO == null) {
	            return null;
	        } else {
	        	Oferta oferta = new Oferta();
	        	oferta.setCodigo(ofertaDTO.getCodigo());
	        	oferta.setProduto(ofertaDTO.getProduto());
	        	oferta.setDescricao(ofertaDTO.getDescricao());
	        	oferta.setStatusOferta(ofertaDTO.getStatusOferta());
	        	oferta.setValorPromocional(ofertaDTO.getValorPromocional());
	        	oferta.setDataValidadeInicio(ofertaDTO.getDataValidade().get(0));
	        	oferta.setDataValidadeFim(ofertaDTO.getDataValidade().get(1));
	            return oferta;
	        }
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
	
	public List<Oferta> ofertasDTOsToOfertas(List<OfertaDTO> ofertaDTOs) {
        return ofertaDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::ofertaDTOToOferta)
            .collect(Collectors.toList());
    }
	
}
