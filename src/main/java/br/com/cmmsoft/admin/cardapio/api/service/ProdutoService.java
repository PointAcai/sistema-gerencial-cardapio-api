package br.com.cmmsoft.admin.cardapio.api.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.cmmsoft.admin.cardapio.api.model.Oferta;
import br.com.cmmsoft.admin.cardapio.api.model.Produto;
import br.com.cmmsoft.admin.cardapio.api.repository.OfertaRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.ProdutoRepository;
import br.com.cmmsoft.admin.cardapio.api.service.dto.ProdutoDTO;
import br.com.cmmsoft.admin.cardapio.api.service.exception.IdentificadorExistenteException;
import br.com.cmmsoft.admin.cardapio.api.service.mapper.ProdutoMapper;
import br.com.cmmsoft.admin.cardapio.api.storage.S3;

@Service
public class ProdutoService {
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private OfertaRepository ofertaRepository;
	
	@Autowired
	private ProdutoMapper produtoMapper;
	
	@Autowired
	private S3 s3;
	
	public ProdutoDTO cadastrar(ProdutoDTO produtoDTO) {
		Produto produto = produtoMapper.produtoDTOToProduto(produtoDTO);
		if (produtoRepository.findByNumIdentificador(produtoDTO.getNumIdentificador()) != null) {
			throw new IdentificadorExistenteException();
		} else {
			produto.setPossuiOferta(false);
			produto.setStatusProduto(true);
			return salvar(produto);
		}
	}
	
	public ProdutoDTO alterar(Long codigo, ProdutoDTO produtoDTO) {
		Produto produtoSalvo = pesquisarProduto(codigo);
		Produto produtoTela = produtoMapper.produtoDTOToProduto(produtoDTO);
		
		if (StringUtils.isEmpty(produtoDTO.getImagem()) && StringUtils.hasText(produtoDTO.getImagem())) {
			s3.remover(produtoSalvo.getImagem());
		} else if (StringUtils.hasText(produtoDTO.getImagem()) && !produtoDTO.getImagem().equals(produtoSalvo.getImagem())) {
			s3.atualizar(produtoSalvo.getImagem(), produtoDTO.getImagem());
		}
		
		BeanUtils.copyProperties(produtoTela, produtoSalvo, "codigo", "statusProduto", "possuiOferta");
		return salvar(produtoSalvo);
	}

	public void alterarStatus(Long codigo, Boolean ativo) {
		Produto produtoSalvo = pesquisarProduto(codigo);
		produtoSalvo.setStatusProduto(ativo);
		produtoRepository.save(produtoSalvo);
	}
	
	public void excluir(Long codigo) {
		Produto produtoSalvo = pesquisarProduto(codigo);
		s3.remover(produtoSalvo.getImagem());
		if (produtoSalvo.getPossuiOferta()) {
			Optional<Oferta> oferta = ofertaRepository.findByProduto(produtoSalvo);
			if (oferta.isPresent()) {
				ofertaRepository.delete(oferta.get());
			}
		}
		produtoRepository.delete(produtoSalvo);
	}

	public ProdutoDTO pesquisarProdutoCodigo(Long codigo) {
		Produto produtoSalvo = pesquisarProduto(codigo);
		if (produtoSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return produtoMapper.produtoToProdutoDTO(produtoSalvo);
	}
	
	/*
	 * Métodos Genéricos
	 * */ 
	
	private Produto pesquisarProduto(Long codigo) {
		return produtoRepository.findOne(codigo);
	}
	
	private ProdutoDTO salvar(Produto produto) {
		
		if (StringUtils.hasText(produto.getImagem())) {
			s3.salvar(produto.getImagem());
		}
		
		return produtoMapper.produtoToProdutoDTO(produtoRepository.save(produto));
	}

}
