package br.com.cmmsoft.admin.cardapio.api.service.dto;

import java.math.BigDecimal;

public class OfertaProdutoDTO {

	/*
	 * ATRIBUTOS
	 * */
	
	private Long numIdentificador;
	private String descricao;
	private BigDecimal valorPromocional;
	private String urlImagem;
	
	/*
	 * GETTERS E SETTERS
	 * */

	public Long getNumIdentificador() {
		return numIdentificador;
	}

	public void setNumIdentificador(Long numIdentificador) {
		this.numIdentificador = numIdentificador;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValorPromocional() {
		return valorPromocional;
	}

	public void setValorPromocional(BigDecimal valorPromocional) {
		this.valorPromocional = valorPromocional;
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}

}
