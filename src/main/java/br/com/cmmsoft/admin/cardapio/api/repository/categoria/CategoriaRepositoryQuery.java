package br.com.cmmsoft.admin.cardapio.api.repository.categoria;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.cmmsoft.admin.cardapio.api.model.Categoria;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoCategoria;

public interface CategoriaRepositoryQuery {
	
	public Page<Categoria> filtrar(Categoria categoriaFilter, Pageable pageable);
	
	public List<ResumoCategoria> resumir();
	
}
