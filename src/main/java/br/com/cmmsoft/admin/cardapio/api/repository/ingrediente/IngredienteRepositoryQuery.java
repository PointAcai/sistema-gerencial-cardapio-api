package br.com.cmmsoft.admin.cardapio.api.repository.ingrediente;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.cmmsoft.admin.cardapio.api.model.Ingrediente;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.IngredienteFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoIngrediente;

public interface IngredienteRepositoryQuery {
	
	public Page<Ingrediente> filtrar(IngredienteFilter ingredienteFilter, Pageable pageable);
	
	public List<ResumoIngrediente> resumir();
}
