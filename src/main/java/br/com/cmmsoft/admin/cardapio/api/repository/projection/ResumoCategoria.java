package br.com.cmmsoft.admin.cardapio.api.repository.projection;

public class ResumoCategoria {
	
	private Long codigo;
	private Long numIdentificador;
	private String descricao;
	private Boolean statusCategoria;
	
	public ResumoCategoria(Long codigo, Long numIdentificador, String descricao, Boolean statusCategoria) {
		super();
		this.codigo = codigo;
		this.numIdentificador = numIdentificador;
		this.descricao = descricao;
		this.statusCategoria = statusCategoria;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	
	public Long getNumIdentificador() {
		return numIdentificador;
	}

	public void setNumIdentificador(Long numIdentificador) {
		this.numIdentificador = numIdentificador;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getStatusCategoria() {
		return statusCategoria;
	}

	public void setStatusCategoria(Boolean statusCategoria) {
		this.statusCategoria = statusCategoria;
	}
	
}
