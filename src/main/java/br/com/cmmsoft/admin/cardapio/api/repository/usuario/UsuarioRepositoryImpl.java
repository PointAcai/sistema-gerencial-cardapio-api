package br.com.cmmsoft.admin.cardapio.api.repository.usuario;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.cmmsoft.admin.cardapio.api.model.Usuario;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.UsuarioFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoUsuario;
import br.com.cmmsoft.admin.cardapio.api.service.dto.UsuarioDTO;
import br.com.cmmsoft.admin.cardapio.api.service.mapper.UsuarioMapper;

/*
 * Classe Responsável por Implementar os Métodos Presentes do UsuarioRepositoryQuery
 * */
public class UsuarioRepositoryImpl implements UsuarioRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
    private UsuarioMapper usuarioMapper;
	
	// Consulta Utilizando a Criteria do JPA
	@Override
	public Page<UsuarioDTO> filtrar(UsuarioFilter usuarioFilter, Pageable pageable) {
		
		// Responsável por Criar uma Criteria
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		
		// Responsável por Criar a Criteria do Objeto
		CriteriaQuery<Usuario> criteria = builder.createQuery(Usuario.class);
		
		// Responsável por Criar o WHERE da Query
		Root<Usuario> root = criteria.from(Usuario.class);
		
		/* Criando as Condições */
		Predicate[] predicates = criarCondicoes(usuarioFilter, builder, root);
		criteria.where(predicates);
		
		// Responsável por Criar a Query da Consulta
		TypedQuery<Usuario> query = manager.createQuery(criteria);
		
		// Responsável por Determinar as Restrições de Paginação da Query
		adicionarCondicoesPaginacao(query, pageable);
		
		return new PageImpl<UsuarioDTO>(usuarioMapper.usuariosToUsuarioDTOs(query.getResultList()), pageable, total(usuarioFilter));
	}
	
	/*
	 * Método Responsável por Determinar um Resumo do Objeto de Usuário (Retornar apenas os Campos Desejados)
	 * */
	@Override
	public List<ResumoUsuario> resumir() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoUsuario> criteria = builder.createQuery(ResumoUsuario.class);
		Root<Usuario> root = criteria.from(Usuario.class);
		
		criteria.select(builder.construct(ResumoUsuario.class, 
				root.get("codigo"), 
				root.get("nome"), 
				root.get("login"),
				root.get("perfil").get("descricao"),
				root.get("status")));
		
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(builder.notEqual(builder.upper(root.get("login")), "ADM_CMM"));
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		
		TypedQuery<ResumoUsuario> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}

	// As Condições são Criadas de Acordo com o que é Passado na Requisição através do Objeto UsuarioFilter
	private Predicate[] criarCondicoes(UsuarioFilter usuarioFilter, CriteriaBuilder builder, Root<Usuario> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		// builder.like = where nome like '%texto%'
		if (!StringUtils.isEmpty(usuarioFilter.getNome())) {
			// 1 Parametro do Builder é o Atributo do Objeto que será Usado na Condição
			// 2 Parametro do Builder utiliza o Simbolo % e complementa com o Valor Passado pelo usuarioFilter
			// builder.lower e toLowerCase = Palavras para Minusculas
			predicates.add(builder.like(
					builder.lower(root.get("nome")),
					"%"+ usuarioFilter.getNome().toLowerCase() +"%"
					));
		}
		
		if (!StringUtils.isEmpty(usuarioFilter.getLogin())) {
			predicates.add(builder.like(
					builder.lower(root.get("login")),
					"%"+ usuarioFilter.getLogin().toLowerCase() +"%"
					));
		}
		
		predicates.add(builder.notEqual(builder.upper(root.get("login")), "ADM_CMM"));
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	// Método Responsável por Determinar as Restrições da Paginação. Ex: Total de Registros e de onde começa.
	private void adicionarCondicoesPaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPagina = pageable.getPageSize();
		int primeiroRegistroPagina = paginaAtual * totalRegistroPagina;
		
		query.setFirstResult(primeiroRegistroPagina);
		query.setMaxResults(totalRegistroPagina);
	}
	
	// Método Responsável por Retornar o Total de Registros
	private Long total(UsuarioFilter usuarioFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<Usuario> root = criteria.from(Usuario.class);
		
		// Utilizar as Condições já Existentes e Aplicar a Paginação também.
		Predicate[] predicates = criarCondicoes(usuarioFilter, builder, root);
		
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		
		return manager.createQuery(criteria).getSingleResult();
	}

}
