package br.com.cmmsoft.admin.cardapio.api.repository.projection;

public class ResumoUnidadeMedida {
	
	private Long codigo;
	private String sigla;
	private String descricao;
	private Boolean status;
	
	// Construtor é Utilizado para Realizar a Consulta pela Query, respeitar a Ordem Definido no RespositoryImpl
	public ResumoUnidadeMedida(Long codigo, String sigla, String descricao, Boolean status) {
		this.codigo = codigo;
		this.sigla = sigla;
		this.descricao = descricao;
		this.status = status;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

}
