package br.com.cmmsoft.admin.cardapio.api.repository.perfil;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.cmmsoft.admin.cardapio.api.model.Perfil;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.PerfilFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoPerfil;

public class PerfilRepositoryImpl implements PerfilRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Perfil> filtrar(PerfilFilter perfilFilter, Pageable pageable) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Perfil> criteria = builder.createQuery(Perfil.class);
		Root<Perfil> root = criteria.from(Perfil.class);
		
		Predicate[] predicates = criarCondicoes(perfilFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Perfil> query = manager.createQuery(criteria);
		adicionarCondicoesPaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(perfilFilter));
	}
	
	@Override
	public List<ResumoPerfil> resumir() {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoPerfil> criteria = builder.createQuery(ResumoPerfil.class);
		Root<Perfil> root = criteria.from(Perfil.class);
		
		criteria.select(builder.construct(ResumoPerfil.class, 
				root.get("codigo"), 
				root.get("descricao"), 
				root.get("status")));
		
		TypedQuery<ResumoPerfil> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}

	private Predicate[] criarCondicoes(PerfilFilter perfilFilter, CriteriaBuilder builder, Root<Perfil> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(perfilFilter.getDescricao())) {
			predicates.add(builder.like(
					builder.lower(root.get("descricao")),
					"%"+ perfilFilter.getDescricao().toLowerCase() +"%"
					));
		}
		
		if (!StringUtils.isEmpty(perfilFilter.getStatus())) {
			predicates.add(builder.equal(
					root.get("status"), 
					perfilFilter.getStatus()
					));
		}
			
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarCondicoesPaginacao(TypedQuery<?> query, Pageable pageable) {
		
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPagina = pageable.getPageSize();
		int primeiroRegistroPagina = paginaAtual * totalRegistroPagina;
		
		query.setFirstResult(primeiroRegistroPagina);
		query.setMaxResults(totalRegistroPagina);
	}
	
	private Long total(PerfilFilter perfilFilter) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<Perfil> root = criteria.from(Perfil.class);
		Predicate[] predicates = criarCondicoes(perfilFilter, builder, root);
		
		criteria.where(predicates);
		criteria.select(builder.count(root));
		
		return manager.createQuery(criteria).getSingleResult();
	}

}
