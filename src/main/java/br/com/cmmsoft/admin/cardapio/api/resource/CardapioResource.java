package br.com.cmmsoft.admin.cardapio.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cmmsoft.admin.cardapio.api.service.CardapioService;
import br.com.cmmsoft.admin.cardapio.api.service.dto.cardapio.CardapioDTO;

@RestController
@RequestMapping("/cardapio")
public class CardapioResource {
		
	@Autowired
	private CardapioService cardapioService;
	
	@GetMapping
	public CardapioDTO pesquisarItensCardapio() {
		return cardapioService.configurarCardapio();
	}
	
}
