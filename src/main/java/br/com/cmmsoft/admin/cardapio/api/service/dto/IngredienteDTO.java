package br.com.cmmsoft.admin.cardapio.api.service.dto;

public class IngredienteDTO {

	/*
	 * ATRIBUTOS
	 * */

	private Long codigo;
	private String descricao;
	
	/*
	 * GETTERS E SETTERS
	 * */
	
	public Long getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
