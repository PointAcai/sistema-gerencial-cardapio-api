/*
 * Classe Responsável por Montar a Query de Consulta do Usuário
 * Baseado nos Parâmetros Passados na Requisição
 * */
package br.com.cmmsoft.admin.cardapio.api.repository.usuario;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.cmmsoft.admin.cardapio.api.repository.filter.UsuarioFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoUsuario;
import br.com.cmmsoft.admin.cardapio.api.service.dto.UsuarioDTO;

// Interface para Apresentar a Anotação do Método que a Classe UsuarioRepositoryImpl irá Implementar
public interface UsuarioRepositoryQuery {
	
	// Anotação de Filtro de Usuário Utilizando Query Nativa e Retornando em um Objeto para Paginação
	public Page<UsuarioDTO> filtrar(UsuarioFilter usuarioFilter, Pageable pageable);
	
	// Anotação para Trazer de Forma Resumida os Dados do Usuário
	public List<ResumoUsuario> resumir();
}
