package br.com.cmmsoft.admin.cardapio.api.listener;

import javax.persistence.PostLoad;

import org.springframework.util.StringUtils;

import br.com.cmmsoft.admin.cardapio.api.CMMAdminCardapioApiApplication;
import br.com.cmmsoft.admin.cardapio.api.model.Parametro;
import br.com.cmmsoft.admin.cardapio.api.storage.S3;

public class ParametroImagemListener {
	
	@PostLoad
	public void postLoad(Parametro parametro) {
		if (StringUtils.hasText(parametro.getValor()) && parametro.getTipo().equals("Imagem")) {
			S3 s3 = CMMAdminCardapioApiApplication.getBean(S3.class);
			parametro.setUrlImagem(s3.configurarUrl(parametro.getValor()));
		}
	}
	
}
