/*
 * Classe Processadora Responsável por Realizar a Interceptação da Requisição na Solicitação do Token de Acesso Fornecido pelo OAuth2
 * Ou seja, ela irá interceptar e Retirar da Resposta da Requisição o REFRESH_TOKEN e Armazena-lo em um Cookie do HTTPS
 * */
package br.com.cmmsoft.admin.cardapio.api.token;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import br.com.cmmsoft.admin.cardapio.api.config.property.CMMAdminCardapioApiProperty;

// Anotação para Poder Escutar Todas as Requisições/Informações realizadas Pelo Controlador OAuth2AccessToken (OAuth2)
@ControllerAdvice
public class RefreshTokenPostProcessor implements ResponseBodyAdvice<OAuth2AccessToken> {

	@Autowired
	private CMMAdminCardapioApiProperty cmmAdminCardapioApiProperty;
	
	/*
	 * A Interceptação acontece quando esse método Retorna TRUE, ou seja, o Método que Retorna TRUE para
	 * essa Interceptação do OAuth2AccessToken é o 'postAcessToken'.
	 * 
	 * Com Isso, no Método beforeBodyWrite consegue Pegar o Corpo da Requisição
	 * */
	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return returnType.getMethod().getName().equals("postAccessToken");
	}
	
	// Método Responsável por Pegar o Corpo da Requisiçãode e Alterar
	@Override
	public OAuth2AccessToken beforeBodyWrite(OAuth2AccessToken body, MethodParameter returnType, MediaType selectContentType,
			Class<? extends HttpMessageConverter<?>> selectConverterType, ServerHttpRequest request, ServerHttpResponse response) {
		
		HttpServletRequest req = ((ServletServerHttpRequest)request).getServletRequest();
		HttpServletResponse res = ((ServletServerHttpResponse)response).getServletResponse();
		
		// Armazena na String o Retorno da Requisição, nesse caso o RefreshToken
		String refreshToken = body.getRefreshToken().getValue();
		
		// Pega o Token da Requisição
		DefaultOAuth2AccessToken token = (DefaultOAuth2AccessToken) body;
		
		// Adiciona o RefreshToken no Cookie
		adicionarRefreshTokenCookie(refreshToken, req, res);
		// Remover o RefreshToken no Body (Corpo de Resposta da Requisição)
		removerRefreshTokenBody(token);
		
		return body;
	}

	// Método Responsável remover o Refresh Token do Corpo da Requisição
	private void removerRefreshTokenBody(DefaultOAuth2AccessToken token) {
		token.setRefreshToken(null);
	}

	// Método Responsável por Armazenar o RefreshToken no Cookie
	private void adicionarRefreshTokenCookie(String refreshToken, HttpServletRequest req, HttpServletResponse res) {
		
		// Criando o Cookie
		Cookie refreshTokenCookie = new Cookie("refreshToken", refreshToken);
		
		// Add Propriedade .setHttpOnly = Cookie apenas de HTTP
		// Add Propriedade .setSecure = Se TRUE segurança apenas de Https (Em Desenvolvimento é FALSE)
		// Add Propriedade .setPath = Para qual Caminho esse Cookie deve ser Enviado 
		// Add Propriedade .setMaxAge = Quanto tempo esse Cookie vai Expirar
		refreshTokenCookie.setHttpOnly(true);
		/*
		 * Configuração realizada no Arquivo application-prod.properties
		 * Ainda não está Funcionando, pois, em Desenvolvimento a Leitura do Arquivo application-prod.properties não é realizada
		 * Por Padrão, como não é lido o arquivo application-prod.properties, o valor de isEnableHttps é FALSE
		 * Esse valor somente será alterado em Produção, ao Subir o Arquivo por Ex: No Heroku, onde será TRUE
		 * */ 
		refreshTokenCookie.setSecure(cmmAdminCardapioApiProperty.getSeguranca().isEnableHttps());
		refreshTokenCookie.setPath(req.getContextPath() + "/oauth/token");
		refreshTokenCookie.setMaxAge(2592000);
		
		// Add o Cookie na Requisição do Response
		res.addCookie(refreshTokenCookie);
	}

}
