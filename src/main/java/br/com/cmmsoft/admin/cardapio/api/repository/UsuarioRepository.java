/*
 * Interface que Contém Vários Métodos Prontos de CRUD
 * */

package br.com.cmmsoft.admin.cardapio.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cmmsoft.admin.cardapio.api.model.Usuario;
import br.com.cmmsoft.admin.cardapio.api.repository.usuario.UsuarioRepositoryQuery;

/*
 * Extend UsuarioRepositoryQuery: Utilizar as Anotações de Métodos Existentes na Interface UsuarioRepositoryQuery
 * Extend JpaRepository: Utilizar as Anotações Presentes do JPA para Persistência dos Dados
 * */  
public interface UsuarioRepository extends JpaRepository<Usuario, Long>, UsuarioRepositoryQuery {
	
	public Optional<Usuario> findByLogin(String login);
	
	public Optional<Usuario> findByNome(String nome);
	
}
