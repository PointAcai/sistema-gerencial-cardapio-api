package br.com.cmmsoft.admin.cardapio.api.service.dto;

public class ImagemDTO {
	
	private String nome;
	private String url;
	
	public ImagemDTO(String nome, String url) {
		this.nome = nome;
		this.url = url;
	}

	public String getNome() {
		return nome;
	}

	public String getUrl() {
		return url;
	}
	
}
