/*
 * 
 * */
package br.com.cmmsoft.admin.cardapio.api.token;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.catalina.util.ParameterMap;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/*
 * Anotação @Order = Tornar esse Filtro uma Propriedade Muita Alta
 * Será Analisado a Requisição Antes de Todo Mundo!
 * */ 
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RefreshTokenCookiePreProcessorFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		
		/*
		 * Verifica se a Requisição é Para o 'OAuth/Token' (Ex: localhost:8080/oauth/token)
		 * Verifica se o "refresh_token" está dentro do Parâmetro "grant_type"
		 * */ 
		if ((("/oauth/token").equalsIgnoreCase(req.getRequestURI()) 
				&& (("refresh_token").equals(req.getParameter("grant_type"))))
			&& (req.getCookies() != null)) {
			// Percorre Todos os Cookies
			for (Cookie cookie : req.getCookies()) {
				// Verifica se Existe o Cookie "refreshToken"
				if (cookie.getName().equals("refreshToken")) {
					// Armazena o Token na Variável que Estava dentro do Cookie
					String refreshToken = cookie.getValue();
					// É preciso Adicionar esse RefreshToken na Requisição
					req = new MyServletRequestWrapper(req, refreshToken);
				}
			}
		}
		
		chain.doFilter(req, response);
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}
	
	/*
	 * Quando a Requisição está Pronta, não pode mais Alterá-la
	 * Então, se faz necessário a criação dessa Classe e Sobrescrever o método getParameterMap()
	 * Para quando a aplicação precisar, passar um outro ParameterMap
	 * */
	static class MyServletRequestWrapper extends HttpServletRequestWrapper {

		private String refreshToken;
		
		public MyServletRequestWrapper(HttpServletRequest request, String refreshToken) {
			super(request);
			this.refreshToken = refreshToken;
		}
		
		@Override
		public Map<String, String[]> getParameterMap() {
			// É Criado um Novo ParameterMap com os Mesmos parâmetros da Requisição Anterior, para poder ser Manipulado
			ParameterMap<String, String[]> map = new ParameterMap<>(getRequest().getParameterMap());
			map.put("refresh_token", new String[] {refreshToken});
			// Trava novamente o MAP na Requisição
			map.setLocked(true);
			return map;
		}
		
	}
	
}
