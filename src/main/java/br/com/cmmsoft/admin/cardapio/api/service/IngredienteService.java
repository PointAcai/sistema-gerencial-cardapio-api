package br.com.cmmsoft.admin.cardapio.api.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.Ingrediente;
import br.com.cmmsoft.admin.cardapio.api.repository.IngredienteRepository;
import br.com.cmmsoft.admin.cardapio.api.service.dto.IngredienteDTO;
import br.com.cmmsoft.admin.cardapio.api.service.mapper.IngredienteMapper;

@Service
public class IngredienteService {
	
	@Autowired
	private IngredienteRepository ingredienteRepository;
	
	@Autowired
	private IngredienteMapper ingredienteMapper;
	
	public Ingrediente cadastrar(IngredienteDTO ingredienteDTO) {
		Ingrediente ingrediente = ingredienteMapper.ingredienteDTOToIngrediente(ingredienteDTO);
		ingrediente.setStatusIngrediente(true);
		return ingredienteRepository.save(ingrediente);
	}
	
	public Ingrediente alterar(Long codigo, IngredienteDTO ingredienteDTO) {
		Ingrediente ingredienteSalvo = pesquisarIngrediente(codigo);
		Ingrediente ingredienteTela = ingredienteMapper.ingredienteDTOToIngrediente(ingredienteDTO);
		BeanUtils.copyProperties(ingredienteTela, ingredienteSalvo, "codigo", "statusIngrediente");
		return salvar(ingredienteSalvo);
	}

	public void alterarStatus(Long codigo, Boolean ativo) {
		Ingrediente ingredienteSalvo = pesquisarIngrediente(codigo);
		ingredienteSalvo.setStatusIngrediente(ativo);
		ingredienteRepository.save(ingredienteSalvo);
	}
	
	public IngredienteDTO pesquisarIngredienteCodigo(Long codigo) {
		Ingrediente ingredienteSalvo = pesquisarIngrediente(codigo);
		if (ingredienteSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return ingredienteMapper.ingredienteToIngredienteDTO(ingredienteSalvo);
	}
	
	/*
	 * Métodos Genéricos
	 * */ 
	
	private Ingrediente pesquisarIngrediente(Long codigo) {
		return ingredienteRepository.findOne(codigo);
	}
	
	private Ingrediente salvar(Ingrediente ingrediente) {
		return ingredienteRepository.save(ingrediente);
	}
	
}
