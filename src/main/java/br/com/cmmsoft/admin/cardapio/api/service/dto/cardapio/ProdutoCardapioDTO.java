package br.com.cmmsoft.admin.cardapio.api.service.dto.cardapio;

import java.math.BigDecimal;

public class ProdutoCardapioDTO {
	
	private Long numIdentificador;
	private String descricao;
	private String ingredientes;
	private BigDecimal valor;
	private Long quantidadeMedida;
	private String unidadeMedida;
	private String urlImagem;
	
	public Long getNumIdentificador() {
		return numIdentificador;
	}
	
	public void setNumIdentificador(Long numIdentificador) {
		this.numIdentificador = numIdentificador;
	}
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getIngredientes() {
		return ingredientes;
	}
	
	public void setIngredientes(String ingredientes) {
		this.ingredientes = ingredientes;
	}
	
	public BigDecimal getValor() {
		return valor;
	}
	
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	public Long getQuantidadeMedida() {
		return quantidadeMedida;
	}
	
	public void setQuantidadeMedida(Long quantidadeMedida) {
		this.quantidadeMedida = quantidadeMedida;
	}
	
	public String getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}
		
}
