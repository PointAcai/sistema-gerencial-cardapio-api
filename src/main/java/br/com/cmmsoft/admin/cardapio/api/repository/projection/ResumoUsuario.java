package br.com.cmmsoft.admin.cardapio.api.repository.projection;

public class ResumoUsuario {
	
	private Long codigo;
	private String nome;
	private String login;
	private String perfil;
	private Boolean status;
	
	// Construtor é Utilizado para Realizar a Consulta pela Query, respeitar a Ordem Definido no RespositoryImpl
	public ResumoUsuario(Long codigo, String nome, String login, String perfil, Boolean status) {
		this.codigo = codigo;
		this.nome = nome;
		this.login = login;
		this.perfil = perfil;
		this.status = status;
	}

	public Long getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	} 
	
	public String getPerfil() {
		return perfil;
	}
	
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
 
}
