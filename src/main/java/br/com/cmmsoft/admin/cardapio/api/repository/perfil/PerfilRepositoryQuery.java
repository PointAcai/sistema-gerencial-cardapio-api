package br.com.cmmsoft.admin.cardapio.api.repository.perfil;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.cmmsoft.admin.cardapio.api.model.Perfil;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.PerfilFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoPerfil;

public interface PerfilRepositoryQuery {
	
	public Page<Perfil> filtrar(PerfilFilter perfilFilter, Pageable pageable);
	
	public List<ResumoPerfil> resumir();
}
