package br.com.cmmsoft.admin.cardapio.api.service.dto.cardapio;

import java.util.ArrayList;
import java.util.List;

public class CategoriaCardapioDTO {

	private String categoria;
	private Long numIdentificador; 
	private List<ProdutoCardapioDTO> produto = new ArrayList<>();
	
	public String getCategoria() {
		return categoria;
	}
	
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	public Long getNumIdentificador() {
		return numIdentificador;
	}

	public void setNumIdentificador(Long numIdentificador) {
		this.numIdentificador = numIdentificador;
	}
	
	public List<ProdutoCardapioDTO> getProduto() {
		return produto;
	}
	
	public void setProduto(List<ProdutoCardapioDTO> produto) {
		this.produto = produto;
	}
	
}
