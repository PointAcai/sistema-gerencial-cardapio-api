/*
 * Classe Responsável por Expor algumas Configurações da API
 * Ex: Segurança Https
 * Profile do Spring, Configuração realizada nos Arquivos de Propriedade da Aplicação
 * É Necessário informar 'ApiApplication' o uso dessa Classe como Profile de Configuração do Projeto - API
 * */
package br.com.cmmsoft.admin.cardapio.api.config.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

// cmmadmincardapio = define no application.properties, propriedades personalizadas
@ConfigurationProperties("cmmadmincardapio")
public class CMMAdminCardapioApiProperty {
	
	private String originPermitida = "http://localhost:4200";
	
	// Configuração da Segurança
	private final Seguranca seguranca = new Seguranca();
	
	// Configuração do S3
	private final S3 s3 = new S3();
	
	public S3 getS3() {
		return s3;
	}

	public Seguranca getSeguranca() {
		return seguranca;
	}
	
	public String getOriginPermitida() {
		return originPermitida;
	}

	public void setOriginPermitida(String originPermitida) {
		this.originPermitida = originPermitida;
	}
	
	public static class S3 {
		
		private String accessKeyId;
		
		private String secretAccessKey;
		
		private String bucket = "aw-cmmadmincardapio-arquivos";

		
		public String getBucket() {
			return bucket;
		}

		public void setBucket(String bucket) {
			this.bucket = bucket;
		}

		public String getAccessKeyId() {
			return accessKeyId;
		}

		public void setAccessKeyId(String accessKeyId) {
			this.accessKeyId = accessKeyId;
		}

		public String getSecretAccessKey() {
			return secretAccessKey;
		}

		public void setSecretAccessKey(String secretAccessKey) {
			this.secretAccessKey = secretAccessKey;
		}
				
	}

	public static class Seguranca {
		
		private boolean enableHttps;

		public boolean isEnableHttps() {
			return enableHttps;
		}

		public void setEnableHttps(boolean enableHttps) {
			this.enableHttps = enableHttps;
		}
	}
	
}
