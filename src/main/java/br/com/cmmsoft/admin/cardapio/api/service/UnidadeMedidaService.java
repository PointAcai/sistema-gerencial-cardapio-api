package br.com.cmmsoft.admin.cardapio.api.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.UnidadeMedida;
import br.com.cmmsoft.admin.cardapio.api.repository.UnidadeMedidaRepository;

@Service
public class UnidadeMedidaService {
	
	@Autowired
	private UnidadeMedidaRepository unidadeMedidaRepository;
	
	public UnidadeMedida cadastrar(UnidadeMedida unidadeMedida) {
		unidadeMedida.setStatusUnidadeMedida(true);
		return unidadeMedidaRepository.save(unidadeMedida);
	}
	
	public UnidadeMedida alterar(Long codigo, UnidadeMedida unidadeMedida) {
		UnidadeMedida unidadeMedidaSalvo = pesquisarUnidadeMedidaCodigo(codigo);
		BeanUtils.copyProperties(unidadeMedida, unidadeMedidaSalvo, "codigo", "statusUnidadeMedida");
		return salvar(unidadeMedidaSalvo);
	}

	public void alterarStatus(Long codigo, Boolean ativo) {
		UnidadeMedida unidadeMedidaSalvo = pesquisarUnidadeMedidaCodigo(codigo);
		unidadeMedidaSalvo.setStatusUnidadeMedida(ativo);
		unidadeMedidaRepository.save(unidadeMedidaSalvo);
	}
	
	private UnidadeMedida pesquisarUnidadeMedidaCodigo(Long codigo) {
		UnidadeMedida unidadeMedidaSalvo = pesquisarUnidadeMedida(codigo);
		if (unidadeMedidaSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return unidadeMedidaSalvo;
	}
	
	/*
	 * Métodos Genéricos
	 * */ 
	
	private UnidadeMedida pesquisarUnidadeMedida(Long codigo) {
		return unidadeMedidaRepository.findOne(codigo);
	}
	
	private UnidadeMedida salvar(UnidadeMedida unidadeMedida) {
		return unidadeMedidaRepository.save(unidadeMedida);
	}
	
}
