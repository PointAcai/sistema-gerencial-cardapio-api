package br.com.cmmsoft.admin.cardapio.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cmmsoft.admin.cardapio.api.model.ControlePedido;
import br.com.cmmsoft.admin.cardapio.api.repository.controlepedido.ControlePedidoRepositoryQuery;

public interface ControlePedidoRepository extends JpaRepository<ControlePedido, Long>, ControlePedidoRepositoryQuery {
	
	public ControlePedido findBySenha(String chave);
	
	public List<ControlePedido> findByStatusControlePedido(Boolean status);
	
	public List<ControlePedido> findTop4ByOrderByStatusControlePedidoDesc();
	
}
