package br.com.cmmsoft.admin.cardapio.api.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.cmmsoft.admin.cardapio.api.event.RecursoCriadoEvent;
import br.com.cmmsoft.admin.cardapio.api.model.UnidadeMedida;
import br.com.cmmsoft.admin.cardapio.api.repository.UnidadeMedidaRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.UnidadeMedidaFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoUnidadeMedida;
import br.com.cmmsoft.admin.cardapio.api.service.UnidadeMedidaService;

@RestController
@RequestMapping("/unidadesmedida")
public class UnidadeMedidaResource {
	
	@Autowired
	private UnidadeMedidaRepository unidadeMedidaRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private UnidadeMedidaService unidadeMedidaService;
		
	// Método Pesquisar com Paginação Através dos Atributos Definidos para Pesquisa
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_UNIDADE_MEDIDA') and #oauth2.hasScope('read')")
	public Page<UnidadeMedida> pesquisar(UnidadeMedidaFilter unidadeMedidaFilter, Pageable pageable) {
		return unidadeMedidaRepository.filtrar(unidadeMedidaFilter, pageable);
	}
	
	// Método Pesquisar de Resumo com Paginação
	@GetMapping(params = "resumo")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_UNIDADE_MEDIDA') and #oauth2.hasScope('read')")
	public List<ResumoUnidadeMedida> resumir() {
		return unidadeMedidaRepository.resumir();
	}
	
	// Método Pesquisar Através do Código
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_UNIDADE_MEDIDA') and #oauth2.hasScope('read')")
	public ResponseEntity<UnidadeMedida> pesquisarUnidadeMedida(@PathVariable Long codigo) {
		UnidadeMedida unidadeMedidaSalvo = unidadeMedidaRepository.findOne(codigo);
		return unidadeMedidaSalvo != null ? ResponseEntity.ok(unidadeMedidaSalvo) : ResponseEntity.notFound().build();
	}
	
	// Método Cadastrar
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_UNIDADE_MEDIDA') and #oauth2.hasScope('write')")
	public ResponseEntity<UnidadeMedida> cadastrar(@Valid @RequestBody UnidadeMedida unidadeMedida, HttpServletResponse response) {
		UnidadeMedida unidadeMedidaSalvo = unidadeMedidaService.cadastrar(unidadeMedida);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, unidadeMedidaSalvo.getCodigo()));
		return ResponseEntity.status(HttpStatus.CREATED).body(unidadeMedidaSalvo);
	}
	
	// Método Excluir
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_EXCLUIR_UNIDADE_MEDIDA') and #oauth2.hasScope('write')")
	public void excluir(@PathVariable Long codigo) {
		unidadeMedidaRepository.delete(codigo);
	}
	
	// Método Alterar
	@PutMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_UNIDADE_MEDIDA') and #oauth2.hasScope('write')")
	public ResponseEntity<UnidadeMedida> alterar(@PathVariable Long codigo, @Valid @RequestBody UnidadeMedida unidadeMedida) {
		UnidadeMedida unidadeMedidaSalvo = unidadeMedidaService.alterar(codigo, unidadeMedida);
		return ResponseEntity.ok(unidadeMedidaSalvo);
	}
	
	// Método Alterar Propriedade Status
	@PutMapping("/{codigo}/status")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_UNIDADE_MEDIDA') and #oauth2.hasScope('write')")
	public void alterarStatus(@PathVariable Long codigo, @RequestBody Boolean ativo) {
		unidadeMedidaService.alterarStatus(codigo, ativo);
	}
	
}
