package br.com.cmmsoft.admin.cardapio.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.Permissao;
import br.com.cmmsoft.admin.cardapio.api.repository.PermissaoRepository;

@Service
public class PermissaoService {
	
	@Autowired
	private PermissaoRepository permissaoRepository;
	
	public List<Permissao> consultarPermissoes() {
		return permissaoRepository.findAll();
	}
	
}
