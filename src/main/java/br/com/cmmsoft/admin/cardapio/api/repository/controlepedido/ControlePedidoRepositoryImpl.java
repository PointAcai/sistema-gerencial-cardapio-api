package br.com.cmmsoft.admin.cardapio.api.repository.controlepedido;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.cmmsoft.admin.cardapio.api.model.ControlePedido;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoControlePedido;

public class ControlePedidoRepositoryImpl implements ControlePedidoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<ControlePedido> filtrar(ControlePedido controlePedido, Pageable pageable) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ControlePedido> criteria = builder.createQuery(ControlePedido.class);
		Root<ControlePedido> root = criteria.from(ControlePedido.class);
		
		Predicate[] predicates = criarCondicoes(controlePedido, builder, root);
		criteria.where(predicates);
		
		TypedQuery<ControlePedido> query = manager.createQuery(criteria);
		adicionarCondicoesPaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(controlePedido));
	}
	
	@Override
	public List<ResumoControlePedido> resumir() {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoControlePedido> criteria = builder.createQuery(ResumoControlePedido.class);
		Root<ControlePedido> root = criteria.from(ControlePedido.class);
		
		List<Order> orderList = new ArrayList<Order>();
		
		criteria.select(
			builder.construct(ResumoControlePedido.class, 
				root.get("codigo"),
				root.get("senha"),
				root.get("nome"), 
				root.get("data"), 
				root.get("statusControlePedido")));
		
		orderList.add(builder.asc(root.get("data")));
		orderList.add(builder.asc(root.get("statusControlePedido")));
		
		criteria.orderBy(orderList);
		
		TypedQuery<ResumoControlePedido> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}

	private Predicate[] criarCondicoes(ControlePedido controlePedido, CriteriaBuilder builder, Root<ControlePedido> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(controlePedido.getSenha())) {
			predicates.add(builder.equal(builder.lower(root.get("senha")), 
					controlePedido.getSenha().toLowerCase()));
		}
		
		if (!StringUtils.isEmpty(controlePedido.getNome())) {
			predicates.add(builder.like(
					builder.lower(root.get("nome")),
					"%"+ controlePedido.getNome().toLowerCase() +"%"
					));
		}
			
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarCondicoesPaginacao(TypedQuery<?> query, Pageable pageable) {
		
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPagina = pageable.getPageSize();
		int primeiroRegistroPagina = paginaAtual * totalRegistroPagina;
		
		query.setFirstResult(primeiroRegistroPagina);
		query.setMaxResults(totalRegistroPagina);
	}
	
	private Long total(ControlePedido controlePedido) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<ControlePedido> root = criteria.from(ControlePedido.class);
		Predicate[] predicates = criarCondicoes(controlePedido, builder, root);
		
		criteria.where(predicates);
		criteria.select(builder.count(root));
		
		return manager.createQuery(criteria).getSingleResult();
	}
	
}
