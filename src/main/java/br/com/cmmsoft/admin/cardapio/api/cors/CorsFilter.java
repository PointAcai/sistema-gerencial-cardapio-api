/*
 * Cros = Objetivo de Controlar o Acesso à Aplicação
 * Classe para Habilitar o CROS no Spring
 * */
package br.com.cmmsoft.admin.cardapio.api.cors;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.cmmsoft.admin.cardapio.api.config.property.CMMAdminCardapioApiProperty;

// Ordem de Prioridade bem Alta, para poder ele Executar desde o Inicio da Aplicação
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CorsFilter implements Filter {

	@Autowired
	private CMMAdminCardapioApiProperty cmmAdminCardapioApiProperty;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		// Define Qual será a Origem que terá Acesso à Aplicação (Configuração a Ser Realizada para Diferentes Ambientes)
		res.setHeader("Access-Control-Allow-Origin", cmmAdminCardapioApiProperty.getOriginPermitida());
		// Define o Modelo de Segurança como Ativo
		res.setHeader("Access-Control-Allow-Credentials", "true");
		
		if (("OPTIONS".equals(req.getMethod())) && (cmmAdminCardapioApiProperty.getOriginPermitida().equals(req.getHeader("Origin")))) {
			// Define os Verbos de Acesso para a Requisição
			res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
			// Define os Parâmetros do Header
			res.setHeader("Access-Control-Allow-Headers", "Authorization, Content-Type, Accept");
			// Define o Tempo de Acesso
			res.setHeader("Access-Control-Max-Age", "3600");
			res.setStatus(HttpServletResponse.SC_OK);
		} else {
			chain.doFilter(request, response);
		}
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
