package br.com.cmmsoft.admin.cardapio.api.repository.parametro;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.cmmsoft.admin.cardapio.api.model.Parametro;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.ParametroFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoParametro;

public class ParametroRepositoryImpl implements ParametroRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Parametro> filtrar(ParametroFilter parametroFilter, Pageable pageable) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Parametro> criteria = builder.createQuery(Parametro.class);
		Root<Parametro> root = criteria.from(Parametro.class);
		
		Predicate[] predicates = criarCondicoes(parametroFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Parametro> query = manager.createQuery(criteria);
		adicionarCondicoesPaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(parametroFilter));
	}
	
	@Override
	public List<ResumoParametro> resumir() {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoParametro> criteria = builder.createQuery(ResumoParametro.class);
		Root<Parametro> root = criteria.from(Parametro.class);
		
		criteria.select(builder.construct(ResumoParametro.class, 
				root.get("codigo"),
				root.get("descricao"),
				root.get("chave"), 
				root.get("valor"), 
				root.get("tipo"),
				root.get("statusParametro")));
		
		TypedQuery<ResumoParametro> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}

	private Predicate[] criarCondicoes(ParametroFilter parametroFilter, CriteriaBuilder builder, Root<Parametro> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(parametroFilter.getChave())) {
			predicates.add(builder.equal(builder.lower(root.get("chave")), 
					parametroFilter.getChave().toLowerCase()));
		}
		
		if (!StringUtils.isEmpty(parametroFilter.getDescricao())) {
			predicates.add(builder.like(
					builder.lower(root.get("descricao")),
					"%"+ parametroFilter.getDescricao().toLowerCase() +"%"
					));
		}
			
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarCondicoesPaginacao(TypedQuery<?> query, Pageable pageable) {
		
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPagina = pageable.getPageSize();
		int primeiroRegistroPagina = paginaAtual * totalRegistroPagina;
		
		query.setFirstResult(primeiroRegistroPagina);
		query.setMaxResults(totalRegistroPagina);
	}
	
	private Long total(ParametroFilter parametroFilter) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<Parametro> root = criteria.from(Parametro.class);
		Predicate[] predicates = criarCondicoes(parametroFilter, builder, root);
		
		criteria.where(predicates);
		criteria.select(builder.count(root));
		
		return manager.createQuery(criteria).getSingleResult();
	}

}
