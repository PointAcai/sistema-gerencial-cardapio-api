package br.com.cmmsoft.admin.cardapio.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cmmsoft.admin.cardapio.api.model.Ingrediente;
import br.com.cmmsoft.admin.cardapio.api.repository.ingrediente.IngredienteRepositoryQuery;

public interface IngredienteRepository extends JpaRepository<Ingrediente, Long>, IngredienteRepositoryQuery {
	
}
