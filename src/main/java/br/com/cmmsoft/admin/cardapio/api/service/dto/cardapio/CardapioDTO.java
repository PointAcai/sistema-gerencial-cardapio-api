package br.com.cmmsoft.admin.cardapio.api.service.dto.cardapio;

import java.util.List;

import br.com.cmmsoft.admin.cardapio.api.model.ControlePedido;
import br.com.cmmsoft.admin.cardapio.api.model.Oferta;

public class CardapioDTO {

	private List<CategoriaCardapioDTO> categorias;
	private List<List<Oferta>> ofertas;
	private List<ControlePedido> controlePedido;

	public List<CategoriaCardapioDTO> getCategorias() {
		return categorias;
	}
	
	public void setCategorias(List<CategoriaCardapioDTO> categorias) {
		this.categorias = categorias;
	}
	
	public List<List<Oferta>> getOfertas() {
		return ofertas;
	}
	
	public void setOfertas(List<List<Oferta>> ofertas) {
		this.ofertas = ofertas;
	}

	public List<ControlePedido> getControlePedido() {
		return controlePedido;
	}

	public void setControlePedido(List<ControlePedido> controlePedido) {
		this.controlePedido = controlePedido;
	}

}
