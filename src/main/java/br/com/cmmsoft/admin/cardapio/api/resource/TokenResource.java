/*
 * Classe Responsável pelo Logoff do Usuário
 * */
package br.com.cmmsoft.admin.cardapio.api.resource;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cmmsoft.admin.cardapio.api.config.property.CMMAdminCardapioApiProperty;

@RestController
@RequestMapping("/tokens")
public class TokenResource {
	
	@Autowired
	private CMMAdminCardapioApiProperty cmmAdminCardapioApiProperty;
	
	@DeleteMapping("/revoke")
	public void revoke(HttpServletRequest request, HttpServletResponse response) {
		
		Cookie cookie = new Cookie("refreshToken", null);
		cookie.setHttpOnly(true);
		/*
		 * Configuração realizada no Arquivo application-prod.properties
		 * Ainda não está Funcionando, pois, em Desenvolvimento a Leitura do Arquivo application-prod.properties não é realizada
		 * Por Padrão, como não é lido o arquivo application-prod.properties, o valor de isEnableHttps é FALSE
		 * Esse valor somente será alterado em Produção, ao Subir o Arquivo por Ex: No Heroku, onde será TRUE
		 * */ 
		cookie.setSecure(cmmAdminCardapioApiProperty.getSeguranca().isEnableHttps());
		cookie.setPath(request.getContextPath() + "/oauth/token");
		cookie.setMaxAge(0);
		
		response.addCookie(cookie);
		response.setStatus(HttpStatus.NO_CONTENT.value());
	}
	
}
