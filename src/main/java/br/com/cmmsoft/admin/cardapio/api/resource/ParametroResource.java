package br.com.cmmsoft.admin.cardapio.api.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.cmmsoft.admin.cardapio.api.event.RecursoCriadoEvent;
import br.com.cmmsoft.admin.cardapio.api.model.Parametro;
import br.com.cmmsoft.admin.cardapio.api.repository.ParametroRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.ParametroFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoParametro;
import br.com.cmmsoft.admin.cardapio.api.service.ParametroService;
import br.com.cmmsoft.admin.cardapio.api.service.dto.ImagemDTO;
import br.com.cmmsoft.admin.cardapio.api.service.dto.ParametroDTO;
import br.com.cmmsoft.admin.cardapio.api.storage.S3;

@RestController
@RequestMapping("/parametros")
public class ParametroResource {
	
	@Autowired
	private ParametroRepository parametroRepository;
	
	@Autowired
	private ParametroService parametroService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private S3 s3;
	
	/*
	 * Serviços do S3
	 * */
	
	@PostMapping("/imagem")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_PARAMETROS') and #oauth2.hasScope('write')")
	public ImagemDTO uploadImagem(@RequestParam MultipartFile valor) {
		String nome = s3.salvarTemporariamente(valor);
		return new ImagemDTO(nome, s3.configurarUrl(nome));
	}
	
	/*
	 * Serviços do Parametro
	 * */
	
	@GetMapping
	public Page<Parametro> pesquisar(ParametroFilter parametroFilter, Pageable pageable) {
		return parametroRepository.filtrar(parametroFilter, pageable);
	}
	
	@GetMapping(params = "resumo")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PARAMETROS') and #oauth2.hasScope('read')")
	public List<ResumoParametro> resumir() {
		return parametroRepository.resumir();
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PARAMETROS') and #oauth2.hasScope('read')")
	public ResponseEntity<ParametroDTO> pesquisarParametroCodigo(@PathVariable Long codigo) {
		ParametroDTO parametroDTO = parametroService.pesquisarParametroCodigo(codigo);
		return parametroDTO != null ? ResponseEntity.ok(parametroDTO) : ResponseEntity.notFound().build();
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_PARAMETROS') and #oauth2.hasScope('write')")
	public ResponseEntity<Parametro> cadastrar(@RequestBody ParametroDTO parametroDTO, HttpServletResponse response) {
		Parametro parametroSalvo = parametroService.cadastrar(parametroDTO);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, parametroSalvo.getCodigo()));
		return ResponseEntity.status(HttpStatus.CREATED).body(parametroSalvo);
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_EXCLUIR_PARAMETROS') and #oauth2.hasScope('write')")
	public void excluir(@PathVariable Long codigo) {
		parametroRepository.delete(codigo);
	}
	
	@PutMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_PARAMETROS') and #oauth2.hasScope('write')")
	public ResponseEntity<Parametro> alterar(@PathVariable Long codigo, @Valid @RequestBody ParametroDTO parametroDTO) {
		Parametro parametroSalvo = parametroService.alterar(codigo, parametroDTO);
		return ResponseEntity.ok(parametroSalvo);
	}
	
	@PutMapping("/{codigo}/status")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_PARAMETROS') and #oauth2.hasScope('write')")
	public void alterarStatus(@PathVariable Long codigo, @RequestBody Boolean ativo) {
		parametroService.alterarStatus(codigo, ativo);
	}
	
}
