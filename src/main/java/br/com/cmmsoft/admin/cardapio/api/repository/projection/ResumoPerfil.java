package br.com.cmmsoft.admin.cardapio.api.repository.projection;

public class ResumoPerfil {
	
	private Long codigo;
	private String descricao;
	private Boolean status;
	
	// Construtor é Utilizado para Realizar a Consulta pela Query, respeitar a Ordem Definido no RespositoryImpl
	public ResumoPerfil(Long codigo, String descricao, Boolean status) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.status = status;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

}
