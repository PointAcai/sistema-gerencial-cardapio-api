package br.com.cmmsoft.admin.cardapio.api.repository.filter;

public class OfertaFilter {
	
	private Long codigo;
	private Long produto;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public Long getProduto() {
		return produto;
	}
	public void setProduto(Long produto) {
		this.produto = produto;
	}

}
