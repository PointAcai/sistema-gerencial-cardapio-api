package br.com.cmmsoft.admin.cardapio.api.repository.controlepedido;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.cmmsoft.admin.cardapio.api.model.ControlePedido;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoControlePedido;

public interface ControlePedidoRepositoryQuery {
	
	public Page<ControlePedido> filtrar(ControlePedido controlePedido, Pageable pageable);
	
	public List<ResumoControlePedido> resumir();
}
