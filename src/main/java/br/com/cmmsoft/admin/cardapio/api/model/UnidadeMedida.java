package br.com.cmmsoft.admin.cardapio.api.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.cmmsoft.admin.cardapio.api.model.converter.BooleanAtivoInativoConverter;

@Entity
@Table(name="T_UNIDADE_MEDIDA")
public class UnidadeMedida {

	/*
	 * ATRIBUTOS
	 * */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codigo")
	private Long codigo;
	
	@Column(name = "sigla")
	private String sigla;
	
	@Column(name = "descricao")
	private String descricao;
	
	@Column(name = "status")
	@Convert(converter = BooleanAtivoInativoConverter.class)
	private Boolean statusUnidadeMedida;
	
	/*
	 * GETTERS E SETTERS
	 * */

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getStatusUnidadeMedida() {
		return statusUnidadeMedida;
	}

	public void setStatusUnidadeMedida(Boolean statusUnidadeMedida) {
		this.statusUnidadeMedida = statusUnidadeMedida;
	}
	
	/*
	 * EQUALS E HASHCODE
	 * */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadeMedida other = (UnidadeMedida) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
}
