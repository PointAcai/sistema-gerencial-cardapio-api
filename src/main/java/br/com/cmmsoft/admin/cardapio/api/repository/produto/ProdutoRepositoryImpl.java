package br.com.cmmsoft.admin.cardapio.api.repository.produto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.cmmsoft.admin.cardapio.api.model.Produto;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.ProdutoFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoProduto;
import br.com.cmmsoft.admin.cardapio.api.service.dto.ProdutoDTO;
import br.com.cmmsoft.admin.cardapio.api.service.mapper.ProdutoMapper;

public class ProdutoRepositoryImpl implements ProdutoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private ProdutoMapper produtoMapper;
	
	@Override
	public Page<ProdutoDTO> filtrar(ProdutoFilter produtoFilter, Pageable pageable) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		
		CriteriaQuery<Produto> criteria = builder.createQuery(Produto.class);
		
		Root<Produto> root = criteria.from(Produto.class);
		
		Predicate[] predicates = criarCondicoes(produtoFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Produto> query = manager.createQuery(criteria);
		
		adicionarCondicoesPaginacao(query, pageable);
		
		return new PageImpl<ProdutoDTO>(produtoMapper.produtosToProdutoDTOs(query.getResultList()), pageable, total(produtoFilter));
	}
	
	@Override
	public List<ResumoProduto> resumir() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoProduto> criteria = builder.createQuery(ResumoProduto.class);
		Root<Produto> root = criteria.from(Produto.class);
		
		criteria.select(builder.construct(ResumoProduto.class, 
				root.get("codigo"), 
				root.get("numIdentificador"),
				root.get("descricao"),
				root.get("categoria").get("descricao"),
				root.get("valor"),
				root.get("statusProduto"),
				root.get("exibirCardapio")));
		
		TypedQuery<ResumoProduto> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	@Override
	public Page<ResumoProduto> pesquisarOfertas(String isOferta) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoProduto> criteria = builder.createQuery(ResumoProduto.class);
		Root<Produto> root = criteria.from(Produto.class);
		
		criteria.select(builder.construct(ResumoProduto.class, 
				root.get("codigo"),
				root.get("numIdentificador"),
				root.get("descricao")));
		
		Predicate[] predicates = criarCondicoesOferta(isOferta, builder, root);
		criteria.where(predicates);
		
		TypedQuery<ResumoProduto> query = manager.createQuery(criteria);
		
		return new PageImpl<>(query.getResultList());
	}
	
	private Predicate[] criarCondicoesOferta(String isOferta, CriteriaBuilder builder, Root<Produto> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if ("SIM".equals(isOferta)) {
			predicates.add(builder.equal(root.get("possuiOferta"), true));
		} else {
			predicates.add(builder.equal(root.get("possuiOferta"), false));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private Predicate[] criarCondicoes(ProdutoFilter produtoFilter, CriteriaBuilder builder, Root<Produto> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(produtoFilter.getCodigo())) {
			predicates.add(builder.equal(
					builder.lower(root.get("numIdentificador")),
						produtoFilter.getCodigo()
					));
		}
		
		if (!StringUtils.isEmpty(produtoFilter.getDescricao())) {
			predicates.add(builder.like(
					builder.lower(root.get("descricao")),
						"%"+ produtoFilter.getDescricao().toLowerCase() +"%"
					));
		}
		
		if (!StringUtils.isEmpty(produtoFilter.getCategoria())) {
			predicates.add(builder.equal(
					builder.lower(root.get("categoria").get("codigo")),
						produtoFilter.getCategoria()
					));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarCondicoesPaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPagina = pageable.getPageSize();
		int primeiroRegistroPagina = paginaAtual * totalRegistroPagina;
		
		query.setFirstResult(primeiroRegistroPagina);
		query.setMaxResults(totalRegistroPagina);
	}
	
	private Long total(ProdutoFilter usuarioFilter) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<Produto> root = criteria.from(Produto.class);
		
		Predicate[] predicates = criarCondicoes(usuarioFilter, builder, root);
		
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		
		return manager.createQuery(criteria).getSingleResult();
	}

}
