package br.com.cmmsoft.admin.cardapio.api.repository.projection;

import java.math.BigDecimal;

public class ResumoProduto {
	
	private Long codigo;
	private Long numIdentificador;
	private String descricao;
	private String categoria;
	private BigDecimal valor;
	private Boolean statusProduto;
	private Boolean exibirCardapio;
	
	public ResumoProduto(Long codigo, Long numIdentificador, String descricao, String categoria, BigDecimal valor, Boolean statusProduto, Boolean exibirCardapio) {
		this.codigo = codigo;
		this.numIdentificador = numIdentificador;
		this.descricao = descricao;
		this.categoria = categoria;
		this.valor = valor;
		this.statusProduto = statusProduto;
		this.exibirCardapio = exibirCardapio;
	}
	
	public ResumoProduto(Long codigo, Long numIdentificador, String descricao) {
		this.codigo = codigo;
		this.numIdentificador = numIdentificador;
		this.descricao = descricao;
	}

	public Long getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	
	public Long getNumIdentificador() {
		return numIdentificador;
	}

	public void setNumIdentificador(Long numIdentificador) {
		this.numIdentificador = numIdentificador;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Boolean getStatusProduto() {
		return statusProduto;
	}

	public void setStatusProduto(Boolean statusProduto) {
		this.statusProduto = statusProduto;
	}

	public Boolean getExibirCardapio() {
		return exibirCardapio;
	}

	public void setExibirCardapio(Boolean exibirCardapio) {
		this.exibirCardapio = exibirCardapio;
	}
	
}
