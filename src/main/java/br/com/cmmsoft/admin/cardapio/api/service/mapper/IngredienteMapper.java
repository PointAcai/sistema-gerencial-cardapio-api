package br.com.cmmsoft.admin.cardapio.api.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.Ingrediente;
import br.com.cmmsoft.admin.cardapio.api.service.dto.IngredienteDTO;

@Service
public class IngredienteMapper {

	public IngredienteDTO ingredienteToIngredienteDTO(Ingrediente ingrediente) {
		IngredienteDTO ingredienteDTO = new IngredienteDTO();
		ingredienteDTO.setCodigo(ingrediente.getCodigo());
		ingredienteDTO.setDescricao(ingrediente.getDescricao());
        return ingredienteDTO;
    }
	
	public List<IngredienteDTO> ingredientesToIngredienteDTOs(List<Ingrediente> ingrediente) {
        return ingrediente.stream()
            .filter(Objects::nonNull)
            .map(this::ingredienteToIngredienteDTO)
            .collect(Collectors.toList());
    }
	
	public Ingrediente ingredienteDTOToIngrediente(IngredienteDTO ingredienteDTO) {
		try {
	        if (ingredienteDTO == null) {
	            return null;
	        } else {
	            Ingrediente ingrediente = new Ingrediente();
	            ingrediente.setCodigo(ingredienteDTO.getCodigo());
	            ingrediente.setDescricao(ingredienteDTO.getDescricao());
	            return ingrediente;
	        }
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
	
	public List<Ingrediente> ingredienteDTOsToIngredientes(List<IngredienteDTO> ingredienteDTOs) {
        return ingredienteDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::ingredienteDTOToIngrediente)
            .collect(Collectors.toList());
    }
	
}
