package br.com.cmmsoft.admin.cardapio.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cmmsoft.admin.cardapio.api.model.UnidadeMedida;
import br.com.cmmsoft.admin.cardapio.api.repository.unidademedida.UnidadeMedidaRepositoryQuery;

public interface UnidadeMedidaRepository extends JpaRepository<UnidadeMedida, Long>, UnidadeMedidaRepositoryQuery {
	
}
