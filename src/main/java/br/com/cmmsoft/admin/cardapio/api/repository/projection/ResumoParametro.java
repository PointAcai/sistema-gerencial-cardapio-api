package br.com.cmmsoft.admin.cardapio.api.repository.projection;

public class ResumoParametro {
	
	private Long codigo;
	private String descricao;
	private String chave;
	private String valor;
	private String tipo;
	private Boolean status;
	private String urlImagem;
	
	// Construtor é Utilizado para Realizar a Consulta pela Query, respeitar a Ordem Definido no RespositoryImpl
	public ResumoParametro(Long codigo, String descricao, String chave, String valor, String tipo, Boolean status) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.chave = chave;
		this.valor = valor;
		this.tipo = tipo;
		this.status = status;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}

}
