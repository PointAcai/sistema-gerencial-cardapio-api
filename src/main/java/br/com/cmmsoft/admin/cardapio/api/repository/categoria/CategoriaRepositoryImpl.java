package br.com.cmmsoft.admin.cardapio.api.repository.categoria;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.cmmsoft.admin.cardapio.api.model.Categoria;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoCategoria;

public class CategoriaRepositoryImpl implements CategoriaRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
		
	@Override
	public Page<Categoria> filtrar(Categoria categoriaFilter, Pageable pageable) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		
		CriteriaQuery<Categoria> criteria = builder.createQuery(Categoria.class);
		
		Root<Categoria> root = criteria.from(Categoria.class);
		
		Predicate[] predicates = criarCondicoes(categoriaFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Categoria> query = manager.createQuery(criteria);
		
		adicionarCondicoesPaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(categoriaFilter));
	}
	
	@Override
	public List<ResumoCategoria> resumir() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoCategoria> criteria = builder.createQuery(ResumoCategoria.class);
		Root<Categoria> root = criteria.from(Categoria.class);
		
		criteria.select(builder.construct(ResumoCategoria.class, 
				root.get("codigo"),
				root.get("numIdentificador"),
				root.get("descricao"),
				root.get("statusCategoria")));
		
		TypedQuery<ResumoCategoria> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}

	private Predicate[] criarCondicoes(Categoria categoriaFilter, CriteriaBuilder builder, Root<Categoria> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(categoriaFilter.getDescricao())) {
			predicates.add(builder.like(
				builder.lower(root.get("descricao")),
				"%"+ categoriaFilter.getDescricao().toLowerCase() +"%"
			));
		}
		
		if (!StringUtils.isEmpty(categoriaFilter.getNumIdentificador())) {
			predicates.add(builder.equal(
				builder.lower(root.get("numIdentificador")),
				categoriaFilter.getNumIdentificador()
			));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarCondicoesPaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPagina = pageable.getPageSize();
		int primeiroRegistroPagina = paginaAtual * totalRegistroPagina;
		
		query.setFirstResult(primeiroRegistroPagina);
		query.setMaxResults(totalRegistroPagina);
	}
	
	private Long total(Categoria categoriaFilter) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<Categoria> root = criteria.from(Categoria.class);
		
		Predicate[] predicates = criarCondicoes(categoriaFilter, builder, root);
		
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		
		return manager.createQuery(criteria).getSingleResult();
	}
	
	/* MÉTODOS GENÉRICOS */
	
//	public Categoria findByNumIdentificador(Long identificador) {
//		CriteriaBuilder builder = manager.getCriteriaBuilder();
//		CriteriaQuery<Categoria> criteria = builder.createQuery(Categoria.class);
//		Root<Categoria> categoria = criteria.from(Categoria.class);
//		criteria.where(builder.equal(categoria.get("numIdentificador"), identificador));
//		TypedQuery<Categoria> query = manager.createQuery(criteria);
//		try {
//			return query.getSingleResult();
//		} catch (NoResultException e) {
//			return null;
//		}
//	}

}
