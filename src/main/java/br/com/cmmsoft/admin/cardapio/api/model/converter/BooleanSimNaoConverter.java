package br.com.cmmsoft.admin.cardapio.api.model.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class BooleanSimNaoConverter implements AttributeConverter<Boolean, String> {

    @Override
    public String convertToDatabaseColumn(Boolean value) {
        return (value != null && value) ? "Sim": "Não";
    }

    @Override
    public Boolean convertToEntityAttribute(String value) {
        if ("Sim".equals(value)) {
            return true;
        } else if ("Não".equals(value)) {
            return false;
        }
        
        return null;
    }

}