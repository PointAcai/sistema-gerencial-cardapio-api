package br.com.cmmsoft.admin.cardapio.api.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.cmmsoft.admin.cardapio.api.event.RecursoCriadoEvent;
import br.com.cmmsoft.admin.cardapio.api.model.Oferta;
import br.com.cmmsoft.admin.cardapio.api.repository.OfertaRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.OfertaFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoOferta;
import br.com.cmmsoft.admin.cardapio.api.service.OfertaService;
import br.com.cmmsoft.admin.cardapio.api.service.dto.OfertaDTO;

@RestController
@RequestMapping("/ofertas")
public class OfertaResource {
	
	@Autowired
	private OfertaRepository ofertaRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private OfertaService ofertaService;
	
	// Método Pesquisar com Paginação Através dos Atributos Definidos para Pesquisa
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_OFERTA') and #oauth2.hasScope('read')")
	public Page<OfertaDTO> pesquisar(OfertaFilter ofertaFilter, Pageable pageable) {
		return ofertaRepository.filtrar(ofertaFilter, pageable);
	}
	
	// Método Pesquisar de Resumo com Paginação
	@GetMapping(params = "resumo")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_OFERTA') and #oauth2.hasScope('read')")
	public List<ResumoOferta> resumir() {
		return ofertaRepository.resumir();
	}
	
	// Método Pesquisar Através do Código
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_OFERTA') and #oauth2.hasScope('read')")
	public ResponseEntity<OfertaDTO> pesquisarUsuarioCodigo(@PathVariable Long codigo) {
		OfertaDTO ofertaSalvo = ofertaService.pesquisarOfertaDTOCodigo(codigo);
		return ofertaSalvo != null ? ResponseEntity.ok(ofertaSalvo) : ResponseEntity.notFound().build();
	}
	
	// Método Cadastrar
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_OFERTA') and #oauth2.hasScope('write')")
	public ResponseEntity<OfertaDTO> cadastrar(@Valid @RequestBody OfertaDTO oferta, HttpServletResponse response) {
		Oferta ofertaSalvo = ofertaService.cadastrar(oferta);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, ofertaSalvo.getCodigo()));
		return ResponseEntity.status(HttpStatus.CREATED).body(oferta);
	}
	
	// Método Excluir
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_EXCLUIR_OFERTA') and #oauth2.hasScope('write')")
	public void excluir(@PathVariable Long codigo) {
		ofertaService.excluir(codigo);
	}
	
	// Método Alterar
	@PutMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_OFERTA') and #oauth2.hasScope('write')")
	public ResponseEntity<Oferta> alterar(@PathVariable Long codigo, @Valid @RequestBody OfertaDTO oferta) {
		Oferta ofertaSalvo = ofertaService.alterar(codigo, oferta);
		return ResponseEntity.ok(ofertaSalvo);
	}
	
	// Método Alterar Propriedade Status
	@PutMapping("/{codigo}/status")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_OFERTA') and #oauth2.hasScope('write')")
	public void alterarStatus(@PathVariable Long codigo, @RequestBody Boolean ativo) {
		ofertaService.alterarStatus(codigo, ativo);
	}
	
}
