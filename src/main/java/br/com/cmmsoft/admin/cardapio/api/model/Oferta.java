package br.com.cmmsoft.admin.cardapio.api.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.cmmsoft.admin.cardapio.api.model.converter.BooleanAtivoInativoConverter;

@Entity
@Table(name = "T_OFERTA")
public class Oferta {
	
	/*
	 * ATRIBUTOS
	 * */
	
	// GenerationType.IDENTITY = MYSQL Responsável pelo Auto Increment
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CODIGO_PRODUTO")
	private Produto produto;
	
	@Column(name = "descricao")
	private String descricao;;
	
	@Column(name = "valor")
	private BigDecimal valorPromocional;
	
	@Column(name = "data_validade_inicio")
	private LocalDateTime dataValidadeInicio;
	
	@Column(name = "data_validade_fim")
	private LocalDateTime dataValidadeFim;
	
	@Column(name = "status")
	@Convert(converter = BooleanAtivoInativoConverter.class)
	private Boolean statusOferta;
			
	/*
	 * GETTERS E SETTERS
	 * */

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValorPromocional() {
		return valorPromocional;
	}

	public void setValorPromocional(BigDecimal valorPromocional) {
		this.valorPromocional = valorPromocional;
	}
	
	public LocalDateTime getDataValidadeInicio() {
		return dataValidadeInicio;
	}

	public void setDataValidadeInicio(LocalDateTime dataValidadeInicio) {
		this.dataValidadeInicio = dataValidadeInicio;
	}

	public LocalDateTime getDataValidadeFim() {
		return dataValidadeFim;
	}

	public void setDataValidadeFim(LocalDateTime dataValidadeFim) {
		this.dataValidadeFim = dataValidadeFim;
	}

	public Boolean getStatusOferta() {
		return statusOferta;
	}

	public void setStatusOferta(Boolean statusOferta) {
		this.statusOferta = statusOferta;
	}
	
	/*
	 * EQUALS E HASHCODE
	 * */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Oferta other = (Oferta) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
}
