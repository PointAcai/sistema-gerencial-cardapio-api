/*
 * Classe que Captura as Exceções de Resposta de Entidades
 * */
package br.com.cmmsoft.admin.cardapio.api.exceptionhandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.cmmsoft.admin.cardapio.api.service.exception.ChaveExistenteException;
import br.com.cmmsoft.admin.cardapio.api.service.exception.IdentificadorExistenteException;

// @ControllerAdvice = Anotação que Observa toda a Aplicação (Controlador da Aplicação) - Vira um Componente do Spring
// Controle Compartilhado e Disponivel para Toda a Aplicação
@ControllerAdvice
public class SisVendaExceptionHandler extends ResponseEntityExceptionHandler {
	
	// Objeto do Spring para Mensagens
	@Autowired
	private MessageSource messageSource;
	
	/*
	 * Método para Tratar Parâmetros da Requisição que não existem na Classe de Modelo da Requisição
	 * (Configuração no Application.properties spring.jackson.deserialization.fail-on-unknown-propertie)
	 * */  
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		String mensagemUsuario = messageSource.getMessage("mensagem.invalida", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.getCause() != null ? ex.getCause().toString() : ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return super.handleExceptionInternal(ex, erros, headers, HttpStatus.BAD_REQUEST, request);
	}
	
	/*
	 * Método para Tratar Parâmetros Inválidos de Acordo com as Validações Existentes. Ex: NotNull
	 * */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		List<Erro> erros = listarErros(ex.getBindingResult());
		return super.handleExceptionInternal(ex, erros, headers, HttpStatus.BAD_REQUEST, request);
	}
	
	/*
	 * Método para Tratar Requisições Determinadas ou Sobrescrever Exceções
	 * Nesse Método por Exemplo: É tratado a Requisição de Dados Vazios/Não Encontrado
	 * Dentro da Anotação @ExceptionHandler basta passar por Parâmetros as Classes de Exceções que Deseja Tratar 
	 * e Sobrescrever a Regra dessa Exceção.
	 * */
	@ExceptionHandler({ EmptyResultDataAccessException.class })
	public ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex, WebRequest request) {
		
		String mensagemUsuario = messageSource.getMessage("recurso.nao-encontrado", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		
		// Devolve ao Response o Status de 'Não Encontrado' caso o Objeto tratado na Requisição não Exista
		return handleExceptionInternal(ex, erros, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
	
	/*
	 * Método para Tratar a Exceção de 
	 * */
	@ExceptionHandler({ DataIntegrityViolationException.class })
	public ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex, WebRequest request) {
		String mensagemUsuario = messageSource.getMessage("recurso.operacao-nao-permitida", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ExceptionUtils.getRootCauseMessage(ex);
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		
		// Devolve ao Response o Status de 'Não Encontrado' caso o Objeto tratado na Requisição não Exista
		return handleExceptionInternal(ex, erros, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
	
	/*
	 * Método para Tratar Exceção de Identificador Existente
	 * */
	@ExceptionHandler({ IdentificadorExistenteException.class })
	public ResponseEntity<Object> handleCategoriaIdentificadorExistenteException(IdentificadorExistenteException ex) {
		String mensagemUsuario = messageSource.getMessage("mensagem.identificador-existente", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}
	
	/*
	 * Método para Tratar Exceção de Chave Existente
	 * */
	@ExceptionHandler({ ChaveExistenteException.class })
	public ResponseEntity<Object> handleChaveExistenteException(ChaveExistenteException ex) {
		String mensagemUsuario = messageSource.getMessage("mensagem.chave-existente", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}
	
	/*
	 * Cria a Lista de Erros, pois Pode Retornar Várias Validações/Erros para Apresentar ao Usuário
	 * */
	private List<Erro> listarErros(BindingResult bindingResult) {
		List<Erro> erros = new ArrayList<>();
		
		for (FieldError fieldError : bindingResult.getFieldErrors()) {
			String mensagemUsuario = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
			String mensagemDesenvolvedor = fieldError.toString();
			erros.add(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		}
		
		return erros;
	}
	
	public static class Erro {
		
		private String mensagemUsuario;
		private String mensagemDesenvolvedor;
		
		public Erro(String mensagemUsuario, String mensagemDesenvolvedor) {
			this.mensagemUsuario = mensagemUsuario;
			this.mensagemDesenvolvedor = mensagemDesenvolvedor;
		}

		public String getMensagemUsuario() {
			return mensagemUsuario;
		}
		
		public String getMensagemDesenvolvedor() {
			return mensagemDesenvolvedor;
		}
	}
	
}
