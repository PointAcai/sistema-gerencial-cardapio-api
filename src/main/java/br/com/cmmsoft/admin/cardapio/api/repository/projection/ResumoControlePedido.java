package br.com.cmmsoft.admin.cardapio.api.repository.projection;

import java.time.LocalDateTime;

public class ResumoControlePedido {
	
	private Long codigo;
	private String senha;
	private String nome;
	private LocalDateTime data;
	private Boolean statusControlePedido;
	
	// Construtor é Utilizado para Realizar a Consulta pela Query, respeitar a Ordem Definido no RespositoryImpl
	public ResumoControlePedido(Long codigo, String senha, String nome, LocalDateTime data, Boolean statusControlePedido) {
		this.codigo = codigo;
		this.senha = senha;
		this.nome = nome;
		this.data = data;
		this.statusControlePedido = statusControlePedido;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDateTime getData() {
		return data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}

	public Boolean getStatusControlePedido() {
		return statusControlePedido;
	}

	public void setStatusControlePedido(Boolean statusControlePedido) {
		this.statusControlePedido = statusControlePedido;
	}

}
