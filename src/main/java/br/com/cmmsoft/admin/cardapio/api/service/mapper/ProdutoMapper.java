package br.com.cmmsoft.admin.cardapio.api.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.Categoria;
import br.com.cmmsoft.admin.cardapio.api.model.Produto;
import br.com.cmmsoft.admin.cardapio.api.model.UnidadeMedida;
import br.com.cmmsoft.admin.cardapio.api.service.dto.ProdutoDTO;

@Service
public class ProdutoMapper {

	public ProdutoDTO produtoToProdutoDTO(Produto produto) {
		ProdutoDTO produtoDTO = new ProdutoDTO();
		produtoDTO.setCodigo(produto.getCodigo());
		produtoDTO.setDescricao(produto.getDescricao());
		produtoDTO.setIngredientes(produto.getIngredientes());
		produtoDTO.setNumIdentificador(produto.getNumIdentificador());
		produtoDTO.setQuantidadeMedida(produto.getQuantidadeMedida());
		produtoDTO.setUnidadeMedida(produto.getUnidadeMedida() == null ? new UnidadeMedida() : produto.getUnidadeMedida());
		produtoDTO.setCategoria(produto.getCategoria().getCodigo() == null ? new Categoria() : produto.getCategoria());
		produtoDTO.setValor(produto.getValor());
		produtoDTO.setExibirCardapio(produto.getExibirCardapio() ? "Sim" : "Nao");
		produtoDTO.setImagem(produto.getImagem());
		produtoDTO.setUrlImagem(produto.getUrlImagem());
        return produtoDTO;
    }
	
	public List<ProdutoDTO> produtosToProdutoDTOs(List<Produto> produto) {
        return produto.stream()
            .filter(Objects::nonNull)
            .map(this::produtoToProdutoDTO)
            .collect(Collectors.toList());
    }
	
	public Produto produtoDTOToProduto(ProdutoDTO produtoDTO) {
		try {
	        if (produtoDTO == null) {
	            return null;
	        } else {
	            Produto produto = new Produto();
	            produto.setCodigo(produtoDTO.getCodigo());
	            produto.setDescricao(produtoDTO.getDescricao());
	            produto.setIngredientes(produtoDTO.getIngredientes());
	            produto.setNumIdentificador(produtoDTO.getNumIdentificador());
	            produto.setQuantidadeMedida(produtoDTO.getQuantidadeMedida());
	            produto.setUnidadeMedida(produtoDTO.getUnidadeMedida().getCodigo() == null ? null : produtoDTO.getUnidadeMedida());
	            produto.setCategoria(produtoDTO.getCategoria().getCodigo() == null ? null : produtoDTO.getCategoria());
	            produto.setValor(produtoDTO.getValor());
	            produto.setExibirCardapio(produtoDTO.getExibirCardapio().equals("Sim") ? true : false);
				produto.setImagem(produtoDTO.getImagem());
	            return produto;
	        }
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
	
	public List<Produto> produtoDTOsToProdutos(List<ProdutoDTO> produtoDTOs) {
        return produtoDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::produtoDTOToProduto)
            .collect(Collectors.toList());
    }
	
}
