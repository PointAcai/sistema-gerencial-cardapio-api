package br.com.cmmsoft.admin.cardapio.api.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.cmmsoft.admin.cardapio.api.service.OfertaService;

@Component
@EnableScheduling
public class SchedulingTask extends Thread {
	
    private final long TEMPO_EXECUCAO = 5000 * 60;
    
    @Autowired
	private OfertaService ofertaService;
	
	@Scheduled(fixedDelay = TEMPO_EXECUCAO)
	public void scheduleFutureTask() {
		
		if (ofertaService != null) {
			ofertaService.encerrarOferta();
		}
	}
	
}