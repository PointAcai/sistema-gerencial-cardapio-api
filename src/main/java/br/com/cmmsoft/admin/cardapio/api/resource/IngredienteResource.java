package br.com.cmmsoft.admin.cardapio.api.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.cmmsoft.admin.cardapio.api.event.RecursoCriadoEvent;
import br.com.cmmsoft.admin.cardapio.api.model.Ingrediente;
import br.com.cmmsoft.admin.cardapio.api.repository.IngredienteRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.IngredienteFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoIngrediente;
import br.com.cmmsoft.admin.cardapio.api.service.IngredienteService;
import br.com.cmmsoft.admin.cardapio.api.service.dto.IngredienteDTO;

@RestController
@RequestMapping("/ingredientes")
public class IngredienteResource {
	
	@Autowired
	private IngredienteRepository ingredienteRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private IngredienteService ingredienteService;
	
	// Método Pesquisar com Paginação Através dos Atributos Definidos para Pesquisa
	@GetMapping
	public Page<Ingrediente> pesquisar(IngredienteFilter ingredienteFilter, Pageable pageable) {
		return ingredienteRepository.filtrar(ingredienteFilter, pageable);
	}
	
	// Método Pesquisar de Resumo com Paginação
	@GetMapping(params = "resumo")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_INGREDIENTE') and #oauth2.hasScope('read')")
	public List<ResumoIngrediente> resumir() {
		return ingredienteRepository.resumir();
	}
	
	// Método Pesquisar Através do Código
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_INGREDIENTE') and #oauth2.hasScope('read')")
	public ResponseEntity<IngredienteDTO> pesquisarUsuarioCodigo(@PathVariable Long codigo) {
		IngredienteDTO ingredienteSalvo = ingredienteService.pesquisarIngredienteCodigo(codigo);
		return ingredienteSalvo != null ? ResponseEntity.ok(ingredienteSalvo) : ResponseEntity.notFound().build();
	}
	
	// Método Cadastrar
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_INGREDIENTE') and #oauth2.hasScope('write')")
	public ResponseEntity<Ingrediente> cadastrar(@Valid @RequestBody IngredienteDTO ingrediente, HttpServletResponse response) {
		Ingrediente ingredienteSalvo = ingredienteService.cadastrar(ingrediente);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, ingredienteSalvo.getCodigo()));
		return ResponseEntity.status(HttpStatus.CREATED).body(ingredienteSalvo);
	}
	
	// Método Excluir
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_EXCLUIR_INGREDIENTE') and #oauth2.hasScope('write')")
	public void excluir(@PathVariable Long codigo) {
		ingredienteRepository.delete(codigo);
	}
	
	// Método Alterar
	@PutMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_INGREDIENTE') and #oauth2.hasScope('write')")
	public ResponseEntity<Ingrediente> alterar(@PathVariable Long codigo, @Valid @RequestBody IngredienteDTO ingrediente) {
		Ingrediente ingredienteSalvo = ingredienteService.alterar(codigo, ingrediente);
		return ResponseEntity.ok(ingredienteSalvo);
	}
	
	// Método Alterar Propriedade Status
	@PutMapping("/{codigo}/status")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_INGREDIENTE') and #oauth2.hasScope('write')")
	public void alterarStatus(@PathVariable Long codigo, @RequestBody Boolean ativo) {
		ingredienteService.alterarStatus(codigo, ativo);
	}
	
}
