package br.com.cmmsoft.admin.cardapio.api.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.cmmsoft.admin.cardapio.api.model.Parametro;
import br.com.cmmsoft.admin.cardapio.api.repository.ParametroRepository;
import br.com.cmmsoft.admin.cardapio.api.service.dto.ParametroDTO;
import br.com.cmmsoft.admin.cardapio.api.service.exception.ChaveExistenteException;
import br.com.cmmsoft.admin.cardapio.api.service.mapper.ParametroMapper;
import br.com.cmmsoft.admin.cardapio.api.storage.S3;

@Service
public class ParametroService {
	
	@Autowired
	private ParametroRepository parametroRepository;
	
	@Autowired
	private ParametroMapper parametroMapper;
	
	@Autowired
	private S3 s3;
	
	public Parametro cadastrar(ParametroDTO parametroDTO) {
		Parametro parametro = parametroMapper.parametroDTOToParametro(parametroDTO);
		if (parametroRepository.findByChave(parametro.getChave()) != null) {
			throw new ChaveExistenteException();
		}
		parametro.setStatusParametro(Boolean.TRUE);
		return salvar(parametro);
	}
	
	public Parametro alterar(Long codigo, ParametroDTO parametroTela) {
		Parametro parametroSalvo = pesquisarParametro(codigo);
		
		if (StringUtils.hasText(parametroTela.getValor()) 
				&& !parametroSalvo.getTipo().equals(parametroTela.getTipo())) {
			s3.remover(parametroSalvo.getValor());
		} else if (StringUtils.hasText(parametroTela.getValor()) && !parametroTela.getValor().equals(parametroSalvo.getValor()) 
				&& parametroTela.getTipo().equals("Imagem")) {
			s3.atualizar(parametroSalvo.getValor(), parametroTela.getValor());
		}
		
		BeanUtils.copyProperties(parametroTela, parametroSalvo, "codigo", "statusParametro");
		return salvar(parametroSalvo);
	}

	public void alterarStatus(Long codigo, Boolean ativo) {
		Parametro parametroSalvo = pesquisarParametro(codigo);
		parametroSalvo.setStatusParametro(ativo);
		parametroRepository.save(parametroSalvo);
	}
	
	public void excluir(Long codigo) {
		Parametro parametroSalvo = pesquisarParametro(codigo);
		s3.remover(parametroSalvo.getValor());
		parametroRepository.delete(parametroSalvo);
	}

	public ParametroDTO pesquisarParametroCodigo(Long codigo) {
		Parametro parametroSalvo = pesquisarParametro(codigo);
		if (parametroSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return parametroMapper.parametroToParametroDTO(parametroSalvo);
	}
	
	/*
	 * Métodos Genéricos
	 * */ 
	
	private Parametro pesquisarParametro(Long codigo) {
		return parametroRepository.findOne(codigo);
	}
	
	private Parametro salvar(Parametro parametro) {
		
		if (StringUtils.hasText(parametro.getValor()) && parametro.getTipo().equals("Imagem")) {
			s3.salvar(parametro.getValor());
		}
		
		return parametroRepository.save(parametro);
	}

}
