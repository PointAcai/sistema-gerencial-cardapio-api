package br.com.cmmsoft.admin.cardapio.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cmmsoft.admin.cardapio.api.model.Permissao;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.PerfilFilter;
import br.com.cmmsoft.admin.cardapio.api.service.PermissaoService;

@RestController
@RequestMapping("/permissoes")
public class PermissaoResource {
	
	@Autowired
	private PermissaoService permissaoService;
	
	// Método Pesquisar de Resumo com Paginação
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PERFIL') and #oauth2.hasScope('read')")
	public List<Permissao> listarPermissoes(PerfilFilter perfilFilter, Pageable pageable) {
		return permissaoService.consultarPermissoes();
	}
	
}
