package br.com.cmmsoft.admin.cardapio.api.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.cmmsoft.admin.cardapio.api.event.RecursoCriadoEvent;
import br.com.cmmsoft.admin.cardapio.api.model.Categoria;
import br.com.cmmsoft.admin.cardapio.api.repository.CategoriaRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoCategoria;
import br.com.cmmsoft.admin.cardapio.api.service.CategoriaService;

@RestController
@RequestMapping("/categorias")
public class CategoriaResource {
	
	@Autowired
	private CategoriaRepository categoriaRepository;
		
	@Autowired
	private ApplicationEventPublisher publisher;
		
	@Autowired
	private CategoriaService categoriaService;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CATEGORIA') and #oauth2.hasScope('read')")
	public Page<Categoria> pesquisar(Categoria categoriaFilter, Pageable pageable) {
		return categoriaRepository.filtrar(categoriaFilter, pageable);
	}
	
	// Retorna o Resumo das Empresas - Apresenta os Dados na Tabela
	@GetMapping(params = "resumo")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CATEGORIA') and #oauth2.hasScope('read')")
	public List<ResumoCategoria> resumir() {
		return categoriaRepository.resumir();
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CATEGORIA') and #oauth2.hasScope('read')")
	public ResponseEntity<Categoria> pesquisarCategoriaCodigo(@PathVariable Long codigo) {
		Categoria categoriaResponse = categoriaService.pesquisarCategoriaCodigo(codigo); 
		return categoriaResponse != null ? ResponseEntity.ok(categoriaResponse) : ResponseEntity.notFound().build();
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_CATEGORIA') and #oauth2.hasScope('write')")
	public ResponseEntity<Categoria> cadastrar(@RequestBody Categoria empresa, HttpServletResponse response) {
		Categoria categoriaResponse = categoriaService.cadastrar(empresa);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, categoriaResponse.getCodigo()));
		return ResponseEntity.status(HttpStatus.CREATED).body(categoriaResponse);
	}
	
	@GetMapping("excluir/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_EXCLUIR_CATEGORIA') and #oauth2.hasScope('write')")
	public Boolean excluir(@PathVariable Long codigo) {
		return categoriaService.excluirCategoria(codigo);
	}
	
	@PutMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_CATEGORIA') and #oauth2.hasScope('write')")
	public ResponseEntity<Categoria> alterar(@PathVariable Long codigo, @RequestBody Categoria categoria) {
		Categoria categoriaResponse = categoriaService.alterar(codigo, categoria);
		return ResponseEntity.ok(categoriaResponse);
	}
	
	@PutMapping("/{codigo}/status")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_CATEGORIA') and #oauth2.hasScope('write')")
	public void alterarStatus(@PathVariable Long codigo, @RequestBody Boolean ativo) {
		categoriaService.alterarStatus(codigo, ativo);
	}
	
}
