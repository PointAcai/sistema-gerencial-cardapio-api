package br.com.cmmsoft.admin.cardapio.api.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.cmmsoft.admin.cardapio.api.model.converter.BooleanAtivoInativoConverter;

@Entity
@Table(name = "T_PERFIL")
public class Perfil {
	
	/*
	 * ATRIBUTOS
	 * */
	
	// GenerationType.IDENTITY = MYSQL Responsável pelo Auto Increment
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	
	// Atribui a Validação NOT NULL para Esse Campo (Usar o @Valid no método de Cadastrar)
	@NotNull
	@Size(min = 3, max = 25)
	@Column(name = "descricao")
	private String descricao;
	
	// Anotação FetchType.EAGER = Toda vez que for Pesquisado o Perfil, já manda também as Permissões desse Perfil 
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "T_PERFIL_PERMISSAO", 
			joinColumns = @JoinColumn(name = "CODIGO_PERFIL"),
			inverseJoinColumns = @JoinColumn(name = "CODIGO_PERMISSAO")
	)
	private Collection<Permissao> permissoes;
	
	@Column(name = "status")
	@Convert(converter = BooleanAtivoInativoConverter.class)
	private Boolean status;
		
	/*
	 * GETTERS E SETTERS
	 * */

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Collection<Permissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Collection<Permissao> permissoes) {
		this.permissoes = permissoes;
	}
	
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	/*
	 * @JsonIgnore = Anotação para o Jackson não Transformar em JSON
	 * @Transient = Anotação para o Hibernate Ignorar a Propriedade
	 * */
	@JsonIgnore
	@Transient
	public boolean isInativo() {
		return !this.getStatus();
	}
	
	/*
	 * EQUALS E HASHCODE
	 * */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Perfil other = (Perfil) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
}
