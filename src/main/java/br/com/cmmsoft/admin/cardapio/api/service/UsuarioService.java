package br.com.cmmsoft.admin.cardapio.api.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.Perfil;
import br.com.cmmsoft.admin.cardapio.api.model.Usuario;
import br.com.cmmsoft.admin.cardapio.api.repository.PerfilRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.UsuarioRepository;
import br.com.cmmsoft.admin.cardapio.api.service.dto.UsuarioDTO;
import br.com.cmmsoft.admin.cardapio.api.service.exception.InativarUsuarioLogadoException;
import br.com.cmmsoft.admin.cardapio.api.service.exception.PerfilInexistenteInativoException;
import br.com.cmmsoft.admin.cardapio.api.service.mapper.UsuarioMapper;

// Classe onde irá conter as Regras de Negócio
/* Anotação para Informar que essa Classe será um Componente do Spring,
 * ou Seja o Spring vai poder Injeta-la onde será preciso usar 
 *  */  
@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private PerfilRepository perfilRepository;
	
	@Autowired
    private UsuarioMapper usuarioMapper;
	
	public UsuarioDTO cadastrar(UsuarioDTO usuarioDTO) {
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		Usuario usuario = usuarioMapper.usuarioDTOToUsuario(usuarioDTO);
		usuario.setStatus(true);
		usuario.setSenha(passwordEncoder.encode(usuarioDTO.getSenha()));
		return salvar(usuario);
	}
	
	public UsuarioDTO alterar(Long codigo, UsuarioDTO usuarioDTO) {
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		Usuario usuarioSalvo = usuarioMapper.usuarioDTOToUsuario(pesquisarUsuarioCodigo(codigo));
		BeanUtils.copyProperties(usuarioDTO, usuarioSalvo, "codigo", "status");
		if (StringUtils.isNotBlank(usuarioDTO.getSenha())) {
			usuarioSalvo.setSenha(passwordEncoder.encode(usuarioDTO.getSenha()));
		}
		return salvar(usuarioSalvo);
	}
	
	public void alterarStatus(Long codigo, Boolean ativo) {
		Usuario usuarioSalvo = pesquisarUsuario(codigo);
		Object nomeUsuario = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (nomeUsuario.equals(usuarioSalvo.getNome())) {
			throw new InativarUsuarioLogadoException();
		}
		usuarioSalvo.setStatus(ativo);
		usuarioRepository.save(usuarioSalvo);
	}

	public UsuarioDTO pesquisarUsuarioCodigo(Long codigo) {
		Usuario usuarioSalvo = pesquisarUsuario(codigo);
		// Caso o Objeto seja Nulo, lança a Exceção de Pelo menos 1 Registro precisa existir
		// Essa Exceção já é tratada pelo SisVendaExceptionHandler
		if (usuarioSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		usuarioSalvo.setSenha(StringUtils.EMPTY);
		return usuarioMapper.usuarioToUsuarioDTO(usuarioSalvo);
	}
	
	/*
	 * Métodos Genéricos
	 * */ 
	
	private Usuario pesquisarUsuario(Long codigo) {
		return usuarioRepository.findOne(codigo);
	}
	
	private UsuarioDTO salvar(Usuario usuario) {
		Perfil perfilSalvo = perfilRepository.findOne(usuario.getPerfil().getCodigo());
		// Verifica se o Perfil Existe ou Está Inativo
		if (perfilSalvo == null || perfilSalvo.isInativo()) {
			throw new PerfilInexistenteInativoException();
		}
		return usuarioMapper.usuarioToUsuarioDTO(usuarioRepository.save(usuario));
	}

}
