package br.com.cmmsoft.admin.cardapio.api.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.cmmsoft.admin.cardapio.api.event.RecursoCriadoEvent;
import br.com.cmmsoft.admin.cardapio.api.model.Ingrediente;
import br.com.cmmsoft.admin.cardapio.api.repository.IngredienteRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.ProdutoRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.ProdutoFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoProduto;
import br.com.cmmsoft.admin.cardapio.api.service.ProdutoService;
import br.com.cmmsoft.admin.cardapio.api.service.dto.ImagemDTO;
import br.com.cmmsoft.admin.cardapio.api.service.dto.ProdutoDTO;
import br.com.cmmsoft.admin.cardapio.api.storage.S3;

@RestController
@RequestMapping("/produtos")
public class ProdutoResource {
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private IngredienteRepository ingredienteRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private S3 s3;
	
	@PostMapping("/imagem")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_PRODUTO') and #oauth2.hasScope('write')")
	public ImagemDTO uploadImagem(@RequestParam MultipartFile imagem) {
		String nome = s3.salvarTemporariamente(imagem);
		return new ImagemDTO(nome, s3.configurarUrl(nome));
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PRODUTO') and #oauth2.hasScope('read')")
	public Page<ProdutoDTO> pesquisar(ProdutoFilter produtoFilter, Pageable pageable) {
		return produtoRepository.filtrar(produtoFilter, pageable);
	}

	@GetMapping(params = "resumo")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PRODUTO') and #oauth2.hasScope('read')")
	public List<ResumoProduto> resumir() {
		return produtoRepository.resumir();
	}
	
	@GetMapping(params = "ofertas")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PRODUTO') and #oauth2.hasScope('read')")
	public Page<ResumoProduto> pesquisarOfertas(String isOferta) {
		return produtoRepository.pesquisarOfertas(isOferta);
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_PRODUTO') and #oauth2.hasScope('write')")
	public ResponseEntity<ProdutoDTO> cadastrar(@RequestBody ProdutoDTO produtoDTO, HttpServletResponse response) {
		ProdutoDTO produtoSalvo = produtoService.cadastrar(produtoDTO);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, produtoSalvo.getNumIdentificador()));
		return ResponseEntity.status(HttpStatus.CREATED).body(produtoSalvo);
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_EXCLUIR_PRODUTO') and #oauth2.hasScope('write')")
	public void excluir(@PathVariable Long codigo) {
		produtoService.excluir(codigo);
	}
	
	@PutMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_PRODUTO') and #oauth2.hasScope('write')")
	public ResponseEntity<ProdutoDTO> alterar(@PathVariable Long codigo, @Valid @RequestBody ProdutoDTO produto) {
		ProdutoDTO produtoSalvo = produtoService.alterar(codigo, produto);
		return ResponseEntity.ok(produtoSalvo);
	}
	
	@PutMapping("/{codigo}/status")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_PRODUTO') and #oauth2.hasScope('write')")
	public void alterarStatus(@PathVariable Long codigo, @RequestBody Boolean ativo) {
		produtoService.alterarStatus(codigo, ativo);
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PRODUTO') and #oauth2.hasScope('read')")
	public ResponseEntity<ProdutoDTO> pesquisarProdutoCodigo(@PathVariable Long codigo) {
		ProdutoDTO produtoSalvo = produtoService.pesquisarProdutoCodigo(codigo);
		return produtoSalvo != null ? ResponseEntity.ok(produtoSalvo) : ResponseEntity.notFound().build();
	}
	
	/* CONSULTA PARA PREENCHIMENTO DAS COMBOS */
	
	// INGREDIENTES
	@GetMapping("/ingredientes")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PRODUTO') and #oauth2.hasScope('read')")
	public List<Ingrediente> pesquisarIngredientes() {
		return ingredienteRepository.findAll();
	}
	
}
