package br.com.cmmsoft.admin.cardapio.api.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.cmmsoft.admin.cardapio.api.listener.ParametroImagemListener;
import br.com.cmmsoft.admin.cardapio.api.model.converter.BooleanAtivoInativoConverter;

@EntityListeners(ParametroImagemListener.class)
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class)
@Entity
@Table(name="T_PARAMETRO")
public class Parametro {

	/*
	 * ATRIBUTOS
	 * */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codigo")
	private Long codigo;

	@Column(name = "descricao")
	private String descricao;
	
	@Column(name = "chave")
	private String chave;
	
	@Column(name = "valor")
	private String valor;
	
	@Column(name = "tipo")
	private String tipo;
	
	@Column(name = "status")
	@Convert(converter = BooleanAtivoInativoConverter.class)
	private Boolean statusParametro;
	
	@Transient
    private String urlImagem;

	/*
	 * GETTERS E SETTERS
	 * */
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Boolean getStatusParametro() {
		return statusParametro;
	}

	public void setStatusParametro(Boolean statusParametro) {
		this.statusParametro = statusParametro;
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}
	
}
