package br.com.cmmsoft.admin.cardapio.api.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.Usuario;
import br.com.cmmsoft.admin.cardapio.api.service.dto.UsuarioDTO;

@Service
public class UsuarioMapper {

	public UsuarioDTO usuarioToUsuarioDTO(Usuario usuario) {
		UsuarioDTO usuarioDTO = new UsuarioDTO();
		usuarioDTO.setCodigo(usuario.getCodigo());
		usuarioDTO.setLogin(usuario.getLogin());
		usuarioDTO.setNome(usuario.getNome());
		usuarioDTO.setSenha(usuario.getSenha());
		usuarioDTO.setPerfil(usuario.getPerfil());
		usuarioDTO.setStatus(usuario.getStatus());
        return usuarioDTO;
    }
	
	public List<UsuarioDTO> usuariosToUsuarioDTOs(List<Usuario> usuario) {
        return usuario.stream()
            .filter(Objects::nonNull)
            .map(this::usuarioToUsuarioDTO)
            .collect(Collectors.toList());
    }
	
	public Usuario usuarioDTOToUsuario(UsuarioDTO usuarioDTO) {
        if (usuarioDTO == null) {
            return null;
        } else {
            Usuario usuario = new Usuario();
            usuario.setCodigo(usuarioDTO.getCodigo());
            usuario.setLogin(usuarioDTO.getLogin());
            usuario.setNome(usuarioDTO.getNome());
            usuario.setPerfil(usuarioDTO.getPerfil());
            usuario.setSenha(usuarioDTO.getSenha());
            usuario.setStatus(usuarioDTO.getStatus());
            return usuario;
        }
    }
	
	public List<Usuario> usuarioDTOsToUsuarios(List<UsuarioDTO> usuarioDTOs) {
        return usuarioDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::usuarioDTOToUsuario)
            .collect(Collectors.toList());
    }
	
}
