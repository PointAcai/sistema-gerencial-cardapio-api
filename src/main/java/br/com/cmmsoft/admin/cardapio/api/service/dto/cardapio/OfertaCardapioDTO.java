package br.com.cmmsoft.admin.cardapio.api.service.dto.cardapio;

import java.util.List;

import br.com.cmmsoft.admin.cardapio.api.model.Oferta;

public class OfertaCardapioDTO {

	private String pagina;
	private List<Oferta> ofertas;

	public String getPagina() {
		return pagina;
	}
	
	public void setPagina(String pagina) {
		this.pagina = pagina;
	}
	
	public List<Oferta> getOfertas() {
		return ofertas;
	}
	
	public void setOfertas(List<Oferta> ofertas) {
		this.ofertas = ofertas;
	}
	
}
