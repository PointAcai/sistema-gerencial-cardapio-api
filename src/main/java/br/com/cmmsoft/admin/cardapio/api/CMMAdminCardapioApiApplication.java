/*
 * Classe que a IDE Gerou para Iniciar a Aplicação Spring Boot
 * */
package br.com.cmmsoft.admin.cardapio.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;

import br.com.cmmsoft.admin.cardapio.api.config.property.CMMAdminCardapioApiProperty;
import br.com.cmmsoft.admin.cardapio.api.scheduler.SchedulingTask;

// Anotação @EnableConfigurationProperties = Habilitar o Uso da Configuração Externamente
@SpringBootApplication
@EnableConfigurationProperties(CMMAdminCardapioApiProperty.class)
public class CMMAdminCardapioApiApplication {
	
	private static ApplicationContext APPLICATION_CONTEXT;

	public static void main(String[] args) {
		APPLICATION_CONTEXT = SpringApplication.run(CMMAdminCardapioApiApplication.class, args);
		
		SchedulingTask scheduling = new SchedulingTask();
		scheduling.scheduleFutureTask();
	}
	
	public static <T> T getBean(Class<T> type) {
		return APPLICATION_CONTEXT.getBean(type);
	}
	
}
