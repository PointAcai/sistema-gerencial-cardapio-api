package br.com.cmmsoft.admin.cardapio.api.repository.unidademedida;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.cmmsoft.admin.cardapio.api.model.UnidadeMedida;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.UnidadeMedidaFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoUnidadeMedida;

public class UnidadeMedidaRepositoryImpl implements UnidadeMedidaRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<UnidadeMedida> filtrar(UnidadeMedidaFilter unidadeMedidaFilter, Pageable pageable) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<UnidadeMedida> criteria = builder.createQuery(UnidadeMedida.class);
		Root<UnidadeMedida> root = criteria.from(UnidadeMedida.class);
		
		Predicate[] predicates = criarCondicoes(unidadeMedidaFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<UnidadeMedida> query = manager.createQuery(criteria);
		adicionarCondicoesPaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(unidadeMedidaFilter));
	}
	
	@Override
	public List<ResumoUnidadeMedida> resumir() {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoUnidadeMedida> criteria = builder.createQuery(ResumoUnidadeMedida.class);
		Root<UnidadeMedida> root = criteria.from(UnidadeMedida.class);
		
		criteria.select(builder.construct(ResumoUnidadeMedida.class, 
				root.get("codigo"),
				root.get("sigla"),
				root.get("descricao"), 
				root.get("statusUnidadeMedida")));
		
		TypedQuery<ResumoUnidadeMedida> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}

	private Predicate[] criarCondicoes(UnidadeMedidaFilter unidadeMedidaFilter, CriteriaBuilder builder, Root<UnidadeMedida> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(unidadeMedidaFilter.getDescricao())) {
			predicates.add(builder.like(
					builder.lower(root.get("descricao")),
					"%"+ unidadeMedidaFilter.getDescricao().toLowerCase() +"%"
					));
		}
		
		if (!StringUtils.isEmpty(unidadeMedidaFilter.getStatus())) {
			predicates.add(builder.equal(
					root.get("informacoesGerais").get("status"), 
					unidadeMedidaFilter.getStatus()
					));
		}
			
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarCondicoesPaginacao(TypedQuery<?> query, Pageable pageable) {
		
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPagina = pageable.getPageSize();
		int primeiroRegistroPagina = paginaAtual * totalRegistroPagina;
		
		query.setFirstResult(primeiroRegistroPagina);
		query.setMaxResults(totalRegistroPagina);
	}
	
	private Long total(UnidadeMedidaFilter unidadeMedidaFilter) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<UnidadeMedida> root = criteria.from(UnidadeMedida.class);
		Predicate[] predicates = criarCondicoes(unidadeMedidaFilter, builder, root);
		
		criteria.where(predicates);
		criteria.select(builder.count(root));
		
		return manager.createQuery(criteria).getSingleResult();
	}

}
