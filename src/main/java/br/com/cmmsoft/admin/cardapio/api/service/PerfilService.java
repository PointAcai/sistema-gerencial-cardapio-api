package br.com.cmmsoft.admin.cardapio.api.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.Perfil;
import br.com.cmmsoft.admin.cardapio.api.repository.PerfilRepository;

@Service
public class PerfilService {
	
	@Autowired
	private PerfilRepository perfilRepository;
	
	public Perfil cadastrar(Perfil perfil) {
		perfil.setStatus(true);
		return perfilRepository.save(perfil);
	}
	
	public Perfil alterar(Long codigo, Perfil perfil) {
		Perfil perfilSalvo = pesquisarPerfilCodigo(codigo);
		BeanUtils.copyProperties(perfil, perfilSalvo, "codigo", "status");
		return salvar(perfilSalvo);
	}

	public void alterarStatus(Long codigo, Boolean ativo) {
		Perfil perfilSalvo = pesquisarPerfilCodigo(codigo);
		perfilSalvo.setStatus(ativo);
		perfilRepository.save(perfilSalvo);
	}
	
	private Perfil pesquisarPerfilCodigo(Long codigo) {
		Perfil perfilSalvo = pesquisarPerfil(codigo);
		if (perfilSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return perfilSalvo;
	}
	
	/*
	 * Métodos Genéricos
	 * */ 
	
	private Perfil pesquisarPerfil(Long codigo) {
		return perfilRepository.findOne(codigo);
	}
	
	private Perfil salvar(Perfil perfil) {
		return perfilRepository.save(perfil);
	}
	
}
