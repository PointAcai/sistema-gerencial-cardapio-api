package br.com.cmmsoft.admin.cardapio.api.repository.unidademedida;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.cmmsoft.admin.cardapio.api.model.UnidadeMedida;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.UnidadeMedidaFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoUnidadeMedida;

public interface UnidadeMedidaRepositoryQuery {
	
	public Page<UnidadeMedida> filtrar(UnidadeMedidaFilter unidadeMedidaFilter, Pageable pageable);
	
	public List<ResumoUnidadeMedida> resumir();
}
