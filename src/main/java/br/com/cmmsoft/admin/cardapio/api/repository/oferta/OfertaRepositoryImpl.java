package br.com.cmmsoft.admin.cardapio.api.repository.oferta;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.cmmsoft.admin.cardapio.api.model.Oferta;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.OfertaFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoOferta;
import br.com.cmmsoft.admin.cardapio.api.service.dto.OfertaDTO;
import br.com.cmmsoft.admin.cardapio.api.service.mapper.OfertaMapper;

public class OfertaRepositoryImpl implements OfertaRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private OfertaMapper ofertaMapper;
	
	@Override
	public Page<OfertaDTO> filtrar(OfertaFilter ofertaFilter, Pageable pageable) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Oferta> criteria = builder.createQuery(Oferta.class);
		Root<Oferta> root = criteria.from(Oferta.class);
		
		Predicate[] predicates = criarCondicoes(ofertaFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Oferta> query = manager.createQuery(criteria);
		adicionarCondicoesPaginacao(query, pageable);
		
		return new PageImpl<OfertaDTO>(ofertaMapper.ofertasToOfertaDTOs(query.getResultList()), pageable, total(ofertaFilter));
	}
	
	@Override
	public List<ResumoOferta> resumir() {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoOferta> criteria = builder.createQuery(ResumoOferta.class);
		Root<Oferta> root = criteria.from(Oferta.class);
		
		criteria.select(builder.construct(ResumoOferta.class, 
				root.get("codigo"),
				root.get("produto").get("descricao"),
				root.get("valorPromocional"),
				root.get("dataValidadeInicio"),
				root.get("dataValidadeFim"),
				root.get("statusOferta")));
		
		TypedQuery<ResumoOferta> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	@Override
	public List<Oferta> pesquisarOfertasAtivas() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Oferta> criteria = builder.createQuery(Oferta.class);
		Root<Oferta> root = criteria.from(Oferta.class);
		
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(builder.equal(builder.upper(root.get("statusOferta")), Boolean.TRUE));
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		
		TypedQuery<Oferta> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}

	private Predicate[] criarCondicoes(OfertaFilter ofertaFilter, CriteriaBuilder builder, Root<Oferta> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(ofertaFilter.getCodigo())) {
			predicates.add(builder.equal(
					builder.lower(root.get("codigo")),
						ofertaFilter.getCodigo()
					));
		}
		
		if (!StringUtils.isEmpty(ofertaFilter.getProduto())) {
			predicates.add(builder.equal(
					builder.lower(root.get("produto").get("codigo")),
					ofertaFilter.getProduto()
					));
		}
			
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarCondicoesPaginacao(TypedQuery<?> query, Pageable pageable) {
		
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPagina = pageable.getPageSize();
		int primeiroRegistroPagina = paginaAtual * totalRegistroPagina;
		
		query.setFirstResult(primeiroRegistroPagina);
		query.setMaxResults(totalRegistroPagina);
	}
	
	private Long total(OfertaFilter ofertaFilter) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<Oferta> root = criteria.from(Oferta.class);
		Predicate[] predicates = criarCondicoes(ofertaFilter, builder, root);
		
		criteria.where(predicates);
		criteria.select(builder.count(root));
		
		return manager.createQuery(criteria).getSingleResult();
	}

}
