package br.com.cmmsoft.admin.cardapio.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cmmsoft.admin.cardapio.api.model.Oferta;
import br.com.cmmsoft.admin.cardapio.api.model.Produto;
import br.com.cmmsoft.admin.cardapio.api.repository.oferta.OfertaRepositoryQuery;

public interface OfertaRepository extends JpaRepository<Oferta, Long>, OfertaRepositoryQuery {
	
	public Optional<Oferta> findByProduto(Produto produto);
	
}
