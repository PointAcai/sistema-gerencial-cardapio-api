package br.com.cmmsoft.admin.cardapio.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cmmsoft.admin.cardapio.api.model.Permissao;

public interface PermissaoRepository extends JpaRepository<Permissao, Long> {
	
}
