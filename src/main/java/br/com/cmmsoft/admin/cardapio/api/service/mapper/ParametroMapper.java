package br.com.cmmsoft.admin.cardapio.api.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.Parametro;
import br.com.cmmsoft.admin.cardapio.api.service.dto.ParametroDTO;

@Service
public class ParametroMapper {

	public ParametroDTO parametroToParametroDTO(Parametro parametro) {
		ParametroDTO parametroDTO = new ParametroDTO();
		parametroDTO.setCodigo(parametro.getCodigo());
		parametroDTO.setChave(parametro.getChave());
		parametroDTO.setDescricao(parametro.getDescricao());
		parametroDTO.setTipo(parametro.getTipo());
		parametroDTO.setValor(parametro.getValor());
		parametroDTO.setUrlImagem(parametro.getUrlImagem());
        return parametroDTO;
    }
	
	public List<ParametroDTO> parametrosToParametroDTOs(List<Parametro> parametro) {
        return parametro.stream()
            .filter(Objects::nonNull)
            .map(this::parametroToParametroDTO)
            .collect(Collectors.toList());
    }
	
	public Parametro parametroDTOToParametro(ParametroDTO parametroDTO) {
		try {
	        if (parametroDTO == null) {
	            return null;
	        } else {
	            Parametro parametro = new Parametro();
	            parametro.setChave(parametroDTO.getChave());
	            parametro.setDescricao(parametroDTO.getDescricao());
	            parametro.setTipo(parametroDTO.getTipo());
	            parametro.setValor(parametroDTO.getValor());
	            return parametro;
	        }
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
	
	public List<Parametro> parametroDTOsToParametros(List<ParametroDTO> parametroDTOs) {
        return parametroDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::parametroDTOToParametro)
            .collect(Collectors.toList());
    }
	
}
