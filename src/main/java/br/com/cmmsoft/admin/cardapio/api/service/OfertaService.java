package br.com.cmmsoft.admin.cardapio.api.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.Oferta;
import br.com.cmmsoft.admin.cardapio.api.model.Produto;
import br.com.cmmsoft.admin.cardapio.api.repository.OfertaRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.ProdutoRepository;
import br.com.cmmsoft.admin.cardapio.api.scheduler.SchedulingTask;
import br.com.cmmsoft.admin.cardapio.api.service.dto.OfertaDTO;
import br.com.cmmsoft.admin.cardapio.api.service.mapper.OfertaMapper;

@Service
public class OfertaService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SchedulingTask.class);
	
	@Autowired
	private OfertaRepository ofertaRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private OfertaMapper ofertaMapper;
	
	public Oferta cadastrar(OfertaDTO ofertaDTO) {
		Optional<Oferta> ofertaExistente = ofertaRepository.findByProduto(ofertaDTO.getProduto());
		if (ofertaExistente.isPresent()) {
			throw new IllegalArgumentException("Já Existe uma Oferta para esse Produto.");
		} else {
			Produto produto = produtoRepository.findOne(ofertaDTO.getProduto().getCodigo());
			produto.setPossuiOferta(true);
			produtoRepository.save(produto);
			ofertaDTO.setStatusOferta(true);
			return ofertaRepository.save(ofertaMapper.ofertaDTOToOferta(ofertaDTO));
		}
	}
	
	public Oferta alterar(Long codigo, OfertaDTO ofertaDTO) {
		Oferta ofertaSalvo = pesquisarOfertaCodigo(codigo);
		BeanUtils.copyProperties(ofertaMapper.ofertaDTOToOferta(ofertaDTO), ofertaSalvo, "codigo", "statusOferta");
		return salvar(ofertaSalvo);
	}
	
	public void excluir(Long codigo) {
		Oferta ofertaSalvo = pesquisarOfertaCodigo(codigo);
		Produto produto = produtoRepository.findOne(ofertaSalvo.getProduto().getCodigo());
		produto.setPossuiOferta(false);
		produtoRepository.save(produto);
		ofertaRepository.delete(codigo);
	}

	public void alterarStatus(Long codigo, Boolean ativo) {
		Oferta ofertaSalvo = pesquisarOfertaCodigo(codigo);
		ofertaSalvo.setStatusOferta(ativo);
		ofertaRepository.save(ofertaSalvo);
	}
	
	private Oferta pesquisarOfertaCodigo(Long codigo) {
		Oferta ofertaSalvo = pesquisarOferta(codigo);
		if (ofertaSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return ofertaSalvo;
	}
	
	public void encerrarOferta() {
		List<Oferta> ofertas = ofertaRepository.findAll();
    	if (!ofertas.isEmpty()) {
    		LOGGER.info("JOB Oferta: Encontrada(s) ["+ofertas.size()+"] oferta(s).");
    		for (Oferta oferta : ofertas) {
    			if (oferta.getDataValidadeFim() != null && oferta.getDataValidadeFim().isBefore(LocalDateTime.now())) {
    				Produto produto = oferta.getProduto();
    				produto.setPossuiOferta(false);
    				produtoRepository.save(produto);
    				ofertaRepository.delete(oferta);
    				LOGGER.info("JOB Oferta: Oferta com o código ["+oferta.getCodigo()+"] encerrada!");
    			}
    		}
    	}
	}
	
	public OfertaDTO pesquisarOfertaDTOCodigo(Long codigo) {
		Oferta ofertaSalvo = pesquisarOferta(codigo);
		if (ofertaSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return ofertaMapper.ofertaToOfertaDTO(ofertaSalvo);
	}
	
	/*
	 * Métodos Genéricos
	 * */ 
	
	private Oferta pesquisarOferta(Long codigo) {
		return ofertaRepository.findOne(codigo);
	}
	
	private Oferta salvar(Oferta oferta) {
		return ofertaRepository.save(oferta);
	}
	
}
