package br.com.cmmsoft.admin.cardapio.api.model;

import java.math.BigDecimal;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.cmmsoft.admin.cardapio.api.listener.ProdutoImagemListener;
import br.com.cmmsoft.admin.cardapio.api.model.converter.BooleanAtivoInativoConverter;
import br.com.cmmsoft.admin.cardapio.api.model.converter.BooleanSimNaoConverter;

@EntityListeners(ProdutoImagemListener.class)
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class)
@Entity
@Table(name="T_PRODUTO")
public class Produto {
	
	/*
	 * ATRIBUTOS
	 * */
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codigo")
	private Long codigo;
	
	@Column(name = "numero_identificador")
	private Long numIdentificador;
	
	@Column(name = "descricao")
	private String descricao;
	
	@Column(name = "quantidade_medida")
	private Long quantidadeMedida;
	
	@ManyToOne
	@JoinColumn(name = "UNIDADE_MEDIDA_CODIGO")
	private UnidadeMedida unidadeMedida;
	
	/*
	 * Produto é o lado dominante, portanto, ao criar um Produto são criados todos os objetos de Ingredientes,
	 * O contrário jamais acontecerá
	 * */
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "T_PRODUTO_INGREDIENTE", 
			joinColumns = @JoinColumn(name = "CODIGO_PRODUTO"),
			inverseJoinColumns = @JoinColumn(name = "CODIGO_INGREDIENTE")
	)
	private Collection<Ingrediente> ingredientes;
	
	@ManyToOne
    @JoinColumn(name = "CATEGORIA_CODIGO")
	private Categoria categoria;
	
	@Column(name = "valor")
	private BigDecimal valor;
	
	@Column(name = "exibir_cardapio")
	@Convert(converter = BooleanSimNaoConverter.class)
	private Boolean exibirCardapio;
	
	@Column(name = "possui_oferta")
	@Convert(converter = BooleanSimNaoConverter.class)
	private Boolean possuiOferta;
	
	@Column(name = "imagem")
    private String imagem;
	
	@Transient
    private String urlImagem;

	@Column(name = "status")
	@Convert(converter = BooleanAtivoInativoConverter.class)
	private Boolean statusProduto;
	
	/*
	 * GETTERS E SETTERS
	 * */

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Long getNumIdentificador() {
		return numIdentificador;
	}

	public void setNumIdentificador(Long numIdentificador) {
		this.numIdentificador = numIdentificador;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getQuantidadeMedida() {
		return quantidadeMedida;
	}

	public void setQuantidadeMedida(Long quantidadeMedida) {
		this.quantidadeMedida = quantidadeMedida;
	}

	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public Collection<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(Collection<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Boolean getExibirCardapio() {
		return exibirCardapio;
	}

	public void setExibirCardapio(Boolean exibirCardapio) {
		this.exibirCardapio = exibirCardapio;
	}

	public Boolean getPossuiOferta() {
		return possuiOferta;
	}

	public void setPossuiOferta(Boolean possuiOferta) {
		this.possuiOferta = possuiOferta;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}

	public Boolean getStatusProduto() {
		return statusProduto;
	}

	public void setStatusProduto(Boolean statusProduto) {
		this.statusProduto = statusProduto;
	}
	
	/*
	 * EQUALS E HASHCODE
	 * */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
}
