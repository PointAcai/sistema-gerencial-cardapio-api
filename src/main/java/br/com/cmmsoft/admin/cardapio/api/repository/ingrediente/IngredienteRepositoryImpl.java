package br.com.cmmsoft.admin.cardapio.api.repository.ingrediente;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.cmmsoft.admin.cardapio.api.model.Ingrediente;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.IngredienteFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoIngrediente;

public class IngredienteRepositoryImpl implements IngredienteRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Ingrediente> filtrar(IngredienteFilter ingredienteFilter, Pageable pageable) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Ingrediente> criteria = builder.createQuery(Ingrediente.class);
		Root<Ingrediente> root = criteria.from(Ingrediente.class);
		
		Predicate[] predicates = criarCondicoes(ingredienteFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Ingrediente> query = manager.createQuery(criteria);
		adicionarCondicoesPaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(ingredienteFilter));
	}
	
	@Override
	public List<ResumoIngrediente> resumir() {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoIngrediente> criteria = builder.createQuery(ResumoIngrediente.class);
		Root<Ingrediente> root = criteria.from(Ingrediente.class);
		
		criteria.select(builder.construct(ResumoIngrediente.class, 
				root.get("codigo"), 
				root.get("descricao"),
				root.get("statusIngrediente")));
		
		TypedQuery<ResumoIngrediente> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}

	private Predicate[] criarCondicoes(IngredienteFilter ingredienteFilter, CriteriaBuilder builder, Root<Ingrediente> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(ingredienteFilter.getDescricao())) {
			predicates.add(builder.like(
					builder.lower(root.get("descricao")),
					"%"+ ingredienteFilter.getDescricao().toLowerCase() +"%"
					));
		}
		
		if (!StringUtils.isEmpty(ingredienteFilter.getStatus())) {
			predicates.add(builder.equal(
					root.get("informacoesGerais").get("status"), 
					ingredienteFilter.getStatus()
					));
		}
			
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarCondicoesPaginacao(TypedQuery<?> query, Pageable pageable) {
		
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPagina = pageable.getPageSize();
		int primeiroRegistroPagina = paginaAtual * totalRegistroPagina;
		
		query.setFirstResult(primeiroRegistroPagina);
		query.setMaxResults(totalRegistroPagina);
	}
	
	private Long total(IngredienteFilter ingredienteFilter) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<Ingrediente> root = criteria.from(Ingrediente.class);
		Predicate[] predicates = criarCondicoes(ingredienteFilter, builder, root);
		
		criteria.where(predicates);
		criteria.select(builder.count(root));
		
		return manager.createQuery(criteria).getSingleResult();
	}

}
