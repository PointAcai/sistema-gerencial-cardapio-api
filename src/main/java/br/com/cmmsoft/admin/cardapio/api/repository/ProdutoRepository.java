package br.com.cmmsoft.admin.cardapio.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cmmsoft.admin.cardapio.api.model.Produto;
import br.com.cmmsoft.admin.cardapio.api.repository.produto.ProdutoRepositoryQuery;

public interface ProdutoRepository extends JpaRepository<Produto, Long>, ProdutoRepositoryQuery {
	
	Produto findByNumIdentificador(Long numIdentificador);
	
}
