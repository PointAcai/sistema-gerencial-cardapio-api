package br.com.cmmsoft.admin.cardapio.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cmmsoft.admin.cardapio.api.model.Perfil;
import br.com.cmmsoft.admin.cardapio.api.repository.perfil.PerfilRepositoryQuery;

public interface PerfilRepository extends JpaRepository<Perfil, Long>, PerfilRepositoryQuery {
	
}
