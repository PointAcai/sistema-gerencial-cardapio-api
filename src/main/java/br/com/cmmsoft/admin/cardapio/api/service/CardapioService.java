package br.com.cmmsoft.admin.cardapio.api.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.Categoria;
import br.com.cmmsoft.admin.cardapio.api.model.ControlePedido;
import br.com.cmmsoft.admin.cardapio.api.model.Ingrediente;
import br.com.cmmsoft.admin.cardapio.api.model.Oferta;
import br.com.cmmsoft.admin.cardapio.api.model.Produto;
import br.com.cmmsoft.admin.cardapio.api.repository.CategoriaRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.ControlePedidoRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.OfertaRepository;
import br.com.cmmsoft.admin.cardapio.api.service.dto.cardapio.CardapioDTO;
import br.com.cmmsoft.admin.cardapio.api.service.dto.cardapio.CategoriaCardapioDTO;
import br.com.cmmsoft.admin.cardapio.api.service.dto.cardapio.ProdutoCardapioDTO;

@Service
public class CardapioService {
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private OfertaRepository ofertaRepository;
	
	@Autowired
	private ControlePedidoRepository controlePedidoRepository;
	
	public CardapioDTO configurarCardapio() {
		
		CardapioDTO cardapio = new CardapioDTO();
		
		cardapio.setCategorias(pesquisarCategoriasEProdutosCardapio());
		cardapio.setOfertas(pesquisarOfertasCardapio());
		cardapio.setControlePedido(pesquisarSenhas());
		return cardapio;
	}
	
	public List<CategoriaCardapioDTO> pesquisarCategoriasEProdutosCardapio() {
		
		List<CategoriaCardapioDTO> categorias = new ArrayList<>();
		CategoriaCardapioDTO categoria;
		ProdutoCardapioDTO produto;
		
		List<Categoria> categoriasBase = categoriaRepository.findAll();
		
		for (Categoria categoriaBase : categoriasBase) {
			if (categoriaBase.getStatusCategoria() && !categoriaBase.getProdutos().isEmpty()) {
				categoria = new CategoriaCardapioDTO();
				categoria.setCategoria(categoriaBase.getDescricao());
				categoria.setNumIdentificador(categoriaBase.getNumIdentificador());
				for (Produto produtoBase : categoriaBase.getProdutos()) {
					if (produtoBase.getStatusProduto() && produtoBase.getExibirCardapio()) {
						produto = new ProdutoCardapioDTO();
						produto.setDescricao(produtoBase.getDescricao());
						produto.setNumIdentificador(produtoBase.getNumIdentificador());
						produto.setQuantidadeMedida(produtoBase.getQuantidadeMedida());
						produto.setUnidadeMedida(produtoBase.getUnidadeMedida() == null ? null : produtoBase.getUnidadeMedida().getSigla());
						produto.setValor(produtoBase.getValor());
						produto.setIngredientes(configurarIngredientes(produtoBase.getIngredientes()));
						produto.setUrlImagem(produtoBase.getUrlImagem());
						categoria.getProduto().add(produto);
					}
				}
				if (!categoria.getProduto().isEmpty()) {
					categorias.add(categoria);
				}
			}
		}
		
		categorias.sort(Comparator.comparing(CategoriaCardapioDTO::getNumIdentificador));
		
		return categorias;
	}

	private String configurarIngredientes(Collection<Ingrediente> ingredientes) {
		
		List<Ingrediente> listaIngredientes = new ArrayList<>(ingredientes);
		
		String descIngredientes = StringUtils.EMPTY;
		
		for (int i = 0; i < listaIngredientes.size(); i++) {
			if (i == 0) {
				descIngredientes = listaIngredientes.get(i).getStatusIngrediente() ? listaIngredientes.get(i).getDescricao() : listaIngredientes.get(i).getDescricao()+" (Em Falta)";
			} else {
				if (i == listaIngredientes.size() - 1) {
					descIngredientes = descIngredientes + " e " + (listaIngredientes.get(i).getStatusIngrediente() ? listaIngredientes.get(i).getDescricao() : listaIngredientes.get(i).getDescricao()+" (Em Falta)");
				} else {
					descIngredientes = descIngredientes + ", " + (listaIngredientes.get(i).getStatusIngrediente() ? listaIngredientes.get(i).getDescricao() : listaIngredientes.get(i).getDescricao()+" (Em Falta)");
				}
			}
		}
		
		return descIngredientes;
	}
	
	public List<List<Oferta>> pesquisarOfertasCardapio() {
		
		List<List<Oferta>> listaOfertas = new ArrayList<>();
		List<Oferta> ofertas = new ArrayList<>();
		
		List<Oferta> ofertasBase = ofertaRepository.pesquisarOfertasAtivas();
		int contador = 1;
		
		for (Oferta oferta : ofertasBase) {
			if (oferta.getStatusOferta()) {
				ofertas.add(oferta);
				if (ofertas.size() == 2 || (ofertas.size() == 1 && contador == ofertasBase.size())) {
					listaOfertas.add(ofertas);
					ofertas = new ArrayList<>();
				}
				contador++;
			}
		}
		
		return listaOfertas;
	}
	
	public List<ControlePedido> pesquisarSenhas() {
		List<ControlePedido> senhas = new ArrayList<>();
		senhas = controlePedidoRepository.findTop4ByOrderByStatusControlePedidoDesc();
		return senhas;
	}
	
}
