package br.com.cmmsoft.admin.cardapio.api.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.cmmsoft.admin.cardapio.api.event.RecursoCriadoEvent;
import br.com.cmmsoft.admin.cardapio.api.model.ControlePedido;
import br.com.cmmsoft.admin.cardapio.api.repository.ControlePedidoRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoControlePedido;
import br.com.cmmsoft.admin.cardapio.api.service.ControlePedidoService;

@RestController
@RequestMapping("/controlepedidos")
public class ControlePedidoResource {
	
	@Autowired
	private ControlePedidoRepository controlePedidoRepository;
	
	@Autowired
	private ControlePedidoService controlePedidoService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	/*
	 * Serviços do ControlePedido
	 * */
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CONTROLE_PEDIDOS') and #oauth2.hasScope('read')")
	public Page<ControlePedido> pesquisar(ControlePedido controlePedido, Pageable pageable) {
		return controlePedidoRepository.filtrar(controlePedido, pageable);
	}
	
	@GetMapping(params = "resumo")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CONTROLE_PEDIDOS') and #oauth2.hasScope('read')")
	public List<ResumoControlePedido> resumir() {
		return controlePedidoRepository.resumir();
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CONTROLE_PEDIDOS') and #oauth2.hasScope('read')")
	public ResponseEntity<ControlePedido> pesquisarControlePedidoCodigo(@PathVariable Long codigo) {
		ControlePedido controlePedido = controlePedidoService.pesquisarControlePedidoCodigo(codigo);
		return controlePedido != null ? ResponseEntity.ok(controlePedido) : ResponseEntity.notFound().build();
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_CONTROLE_PEDIDOS') and #oauth2.hasScope('write')")
	public ResponseEntity<ControlePedido> cadastrar(@RequestBody ControlePedido controlePedidoDTO, HttpServletResponse response) {
		ControlePedido controlePedidoSalvo = controlePedidoService.cadastrar(controlePedidoDTO);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, controlePedidoSalvo.getCodigo()));
		return ResponseEntity.status(HttpStatus.CREATED).body(controlePedidoSalvo);
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_EXCLUIR_CONTROLE_PEDIDOS') and #oauth2.hasScope('write')")
	public void excluir(@PathVariable Long codigo) {
		controlePedidoRepository.delete(codigo);
	}
	
	@PutMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_CONTROLE_PEDIDOS') and #oauth2.hasScope('write')")
	public ResponseEntity<ControlePedido> alterar(@PathVariable Long codigo, @Valid @RequestBody ControlePedido controlePedidoDTO) {
		ControlePedido controlePedidoSalvo = controlePedidoService.alterar(codigo, controlePedidoDTO);
		return ResponseEntity.ok(controlePedidoSalvo);
	}
	
	@PutMapping("/{codigo}/status")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_CONTROLE_PEDIDOS') and #oauth2.hasScope('write')")
	public void alterarStatus(@PathVariable Long codigo, @RequestBody Boolean ativo) {
		controlePedidoService.alterarStatus(codigo, ativo);
	}
	
}
