package br.com.cmmsoft.admin.cardapio.api.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.ControlePedido;
import br.com.cmmsoft.admin.cardapio.api.repository.ControlePedidoRepository;

@Service
public class ControlePedidoService {
	
	@Autowired
	private ControlePedidoRepository controlePedidoRepository;
	
	public ControlePedido cadastrar(ControlePedido controlePedido) {
		controlePedido.setData(LocalDateTime.now());
		controlePedido.setStatusControlePedido(Boolean.FALSE);
		return this.salvar(controlePedido);
	}
	
	public ControlePedido alterar(Long codigo, ControlePedido controlePedidoTela) {
		ControlePedido controlePedidoSalvo = pesquisarControlePedido(codigo);
		BeanUtils.copyProperties(controlePedidoTela, controlePedidoSalvo, "codigo", "statusControlePedido", "data");
		return this.salvar(controlePedidoSalvo);
	}

	public void alterarStatus(Long codigo, Boolean ativo) {
		ControlePedido controlePedidoSalvo = pesquisarControlePedido(codigo);
		controlePedidoSalvo.setStatusControlePedido(ativo);
		
		List<ControlePedido> controlePedidos = controlePedidoRepository.findByStatusControlePedido(Boolean.TRUE);
		for (ControlePedido controle : controlePedidos) {
			controle.setStatusControlePedido(Boolean.FALSE);
			this.salvar(controle);
		}
		
		controlePedidoRepository.save(controlePedidoSalvo);
	}
	
	public void excluir(Long codigo) {
		ControlePedido controlePedidoSalvo = pesquisarControlePedido(codigo);
		controlePedidoRepository.delete(controlePedidoSalvo);
	}

	public ControlePedido pesquisarControlePedidoCodigo(Long codigo) {
		ControlePedido controlePedidoSalvo = pesquisarControlePedido(codigo);
		if (controlePedidoSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return controlePedidoSalvo;
	}
	
	/*
	 * Métodos Genéricos
	 * */ 
	
	private ControlePedido pesquisarControlePedido(Long codigo) {
		return controlePedidoRepository.findOne(codigo);
	}
	
	private ControlePedido salvar(ControlePedido controlePedido) {
		return controlePedidoRepository.save(controlePedido);
	}

}
