package br.com.cmmsoft.admin.cardapio.api.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.Categoria;
import br.com.cmmsoft.admin.cardapio.api.repository.CategoriaRepository;
import br.com.cmmsoft.admin.cardapio.api.service.exception.IdentificadorExistenteException;

@Service
public class CategoriaService {
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public Categoria cadastrar(Categoria categoriaRequest) {
		if (categoriaRepository.findByNumIdentificador(categoriaRequest.getNumIdentificador()) != null) {
			throw new IdentificadorExistenteException();
		} else {
			categoriaRequest.setStatusCategoria(true);
			return salvar(categoriaRequest);
		}
	}
	
	public Categoria alterar(Long codigo, Categoria empresa) {
		Categoria categoriaSalva = pesquisarCategoria(codigo);
		BeanUtils.copyProperties(empresa, categoriaSalva, "codigo", "statusCategoria", "produtos");
		return salvar(categoriaSalva);
	}
	
	public void alterarStatus(Long codigo, Boolean ativo) {
		Categoria categoriaSalva = pesquisarCategoria(codigo);
		categoriaSalva.setStatusCategoria(ativo);
		categoriaRepository.save(categoriaSalva);
	}

	public Categoria pesquisarCategoriaCodigo(Long codigo) {
		Categoria categoriaSalva = pesquisarCategoria(codigo);
		if (categoriaSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return categoriaSalva;
	}
	
	public Boolean excluirCategoria(Long codigo) {
		if (!hasProduto(codigo)) {
			categoriaRepository.delete(codigo);
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}
	
	private Boolean hasProduto(Long codigo) {
		String sql = "SELECT COUNT(*) FROM T_PRODUTO WHERE CATEGORIA_CODIGO = ?";
		int count = jdbcTemplate.queryForObject(sql, new Object[] { codigo }, Integer.class);
		return count > 0;
	}
	
	/*
	 * Métodos Genéricos
	 * */ 
	
	private Categoria pesquisarCategoria(Long codigo) {
		return categoriaRepository.findOne(codigo);
	}
	
	private Categoria salvar(Categoria categoria) {
		return categoriaRepository.save(categoria);
	}

}
