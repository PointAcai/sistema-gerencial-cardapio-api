package br.com.cmmsoft.admin.cardapio.api.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.cmmsoft.admin.cardapio.api.model.converter.BooleanAtivoInativoConverter;

@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class)
@Entity
@Table(name="T_CONTROLE_PEDIDO")
public class ControlePedido {

	/*
	 * ATRIBUTOS
	 * */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codigo")
	private Long codigo;
	
	@Column(name = "senha")
	private String senha;
	
	@Column(name = "data")
	private LocalDateTime data;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "status")
	@Convert(converter = BooleanAtivoInativoConverter.class)
	private Boolean statusControlePedido;
	
	/*
	 * GETTERS E SETTERS
	 * */

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public LocalDateTime getData() {
		return data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getStatusControlePedido() {
		return statusControlePedido;
	}

	public void setStatusControlePedido(Boolean statusControlePedido) {
		this.statusControlePedido = statusControlePedido;
	}
	
}
