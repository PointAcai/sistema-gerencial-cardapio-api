package br.com.cmmsoft.admin.cardapio.api.event.listener;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.cmmsoft.admin.cardapio.api.event.RecursoCriadoEvent;

/*
 * Classe que fica Ouvindo o RecursoCriadoEvent (Toda vez que chamar o RecursoCriadoEvent, essa Classe vai tratar o Evento)
 * */

// Anotação para Informar que essa Classe é um Component do Spring
@Component
public class RecursoCriadoEventListener implements ApplicationListener<RecursoCriadoEvent>  {

	@Override
	public void onApplicationEvent(RecursoCriadoEvent recursoCriadoEvent) {
		HttpServletResponse response = recursoCriadoEvent.getResponse();
		
		// Recebe o Evento e Adiciona no Location
		Long codigo = recursoCriadoEvent.getCodigo();
		
		adicionarHeaderLocation(response, codigo);
	}

	private void adicionarHeaderLocation(HttpServletResponse response, Long codigo) {
		
		// Para Boa Prática do REST, informar depois que Criado, como Recuperar esse Registro no Header dentro do Location
		// Pega a partir da Requisição Atual e Adicionar o Código na URI
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").
				buildAndExpand(codigo).toUri();
		
		response.setHeader("Location", uri.toASCIIString());
	}
	
}
