package br.com.cmmsoft.admin.cardapio.api.repository.projection;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ResumoOferta {
	
	private Long codigo;
	private String descricao;
	private BigDecimal valorPromocional;
	private LocalDateTime dataValidadeInicio;
	private LocalDateTime dataValidadeFim;
	private Boolean statusOferta;
	
	// Construtor é Utilizado para Realizar a Consulta pela Query, respeitar a Ordem Definido no RespositoryImpl
	public ResumoOferta(Long codigo, String descricao, BigDecimal valorPromocional, 
			LocalDateTime dataValidadeInicio, LocalDateTime dataValidadeFim, Boolean statusOferta) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.valorPromocional = valorPromocional;
		this.dataValidadeInicio = dataValidadeInicio;
		this.dataValidadeFim = dataValidadeFim;
		this.statusOferta = statusOferta;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValorPromocional() {
		return valorPromocional;
	}

	public void setValorPromocional(BigDecimal valorPromocional) {
		this.valorPromocional = valorPromocional;
	}

	public LocalDateTime getDataValidadeInicio() {
		return dataValidadeInicio;
	}

	public void setDataValidadeInicio(LocalDateTime dataValidadeInicio) {
		this.dataValidadeInicio = dataValidadeInicio;
	}

	public LocalDateTime getDataValidadeFim() {
		return dataValidadeFim;
	}

	public void setDataValidadeFim(LocalDateTime dataValidadeFim) {
		this.dataValidadeFim = dataValidadeFim;
	}

	public Boolean getStatusOferta() {
		return statusOferta;
	}

	public void setStatusOferta(Boolean statusOferta) {
		this.statusOferta = statusOferta;
	}

}
