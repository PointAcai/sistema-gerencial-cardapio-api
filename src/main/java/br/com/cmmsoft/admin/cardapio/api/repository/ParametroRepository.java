package br.com.cmmsoft.admin.cardapio.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cmmsoft.admin.cardapio.api.model.Parametro;
import br.com.cmmsoft.admin.cardapio.api.repository.parametro.ParametroRepositoryQuery;

public interface ParametroRepository extends JpaRepository<Parametro, Long>, ParametroRepositoryQuery {
	
	Parametro findByChave(String chave);
	
}
