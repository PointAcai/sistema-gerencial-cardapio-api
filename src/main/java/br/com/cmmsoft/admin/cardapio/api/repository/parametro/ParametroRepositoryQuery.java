package br.com.cmmsoft.admin.cardapio.api.repository.parametro;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.cmmsoft.admin.cardapio.api.model.Parametro;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.ParametroFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoParametro;

public interface ParametroRepositoryQuery {
	
	public Page<Parametro> filtrar(ParametroFilter parametroFilter, Pageable pageable);
	
	public List<ResumoParametro> resumir();
}
