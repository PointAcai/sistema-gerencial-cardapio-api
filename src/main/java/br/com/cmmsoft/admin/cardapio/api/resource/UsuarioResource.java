/*
 * Classe de Recurso que vai Expor Tudo que Esteja Relacionado a Usuario
 * Controlador Recebe as Requisições pelo Spring 
 * Não se Pode botar nenhum Tipo de Regra no Controlador!
 * */
package br.com.cmmsoft.admin.cardapio.api.resource;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.cmmsoft.admin.cardapio.api.event.RecursoCriadoEvent;
import br.com.cmmsoft.admin.cardapio.api.exceptionhandler.SisVendaExceptionHandler.Erro;
import br.com.cmmsoft.admin.cardapio.api.repository.UsuarioRepository;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.UsuarioFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoUsuario;
import br.com.cmmsoft.admin.cardapio.api.service.UsuarioService;
import br.com.cmmsoft.admin.cardapio.api.service.dto.UsuarioDTO;
import br.com.cmmsoft.admin.cardapio.api.service.exception.InativarUsuarioLogadoException;
import br.com.cmmsoft.admin.cardapio.api.service.exception.PerfilInexistenteInativoException;

// Anotação para que o Retorno seja Convertida para JSON
@RestController
// Anotação do Mapeamento da Requisição REST para a Classe Usuario
@RequestMapping("/usuarios")
public class UsuarioResource {
	
	// Anotação de Injeção de Usuario, pois é uma Interface, não dar pra Instanciar
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	// Anotação de Publicação de Eventos da Aplicação
	@Autowired
	private ApplicationEventPublisher publisher;
	
	// Anotação da Classe onde terá as Regras de Negócio da Aplicação
	@Autowired
	private UsuarioService usuarioService;
	
	// Anotação para Implementar a Mensagem de Exceção de Não Existir Perfil ou o Perfil Está Inativo
	@Autowired
	private MessageSource messageSource;
	
	// GET (Verbo) = Anotação de Método/Função para CONSULTAR Usuario
	// Recebe como Parâmetro o Filtro na Requisição de Acordo com os Campos Mapeados no Objeto UsuarioFilter
	// Atributo Pageable = Paginação: Recebe os Parâmetros da Requisição 'size e page'. Ex: ?size=3&page=2  
	// Atributo #oauth2.hasScope('read') = Determina que esse Método terá apenas Scopo de READ
	/*
	 * ANOTAÇÃO: hasAuthority('ROLE_PESQUISAR_USUARIO') é a Permissão do Usuário Logado, 
	 * configurado em AppUserDetailsService no método getPermissoes!
	 * ANOTAÇÃO: #oauth2.hasScope('read') é a Permissão do Cliente que está Usando a Aplicação!
	 * */
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_USUARIO') and #oauth2.hasScope('read')")
	public Page<UsuarioDTO> pesquisar(UsuarioFilter usuarioFilter, Pageable pageable) {
		return usuarioRepository.filtrar(usuarioFilter, pageable);
	}
	
	/*
	 * Método Responsável por Pesquisar o Usuário de Forma Resumida
	 * Utilizando o Params = 'resumo' na Requisição do GET
	 * */
	@GetMapping(params = "resumo")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_USUARIO') and #oauth2.hasScope('read')")
	public List<ResumoUsuario> resumir() {
		return usuarioRepository.resumir();
	}
	
	// POST (Verbo) = Anotação de Método/Função para SALVAR Usuario
	// Anotação @RequestBody Converte o que está Passando do JSON para o Objeto em Java
	// Anotação @Valid serve para Utilizar as Validações aplicadas na Classe de Modelo (Ex: NotNull do Campo Nome)
	// Atributo #oauth2.hasScope('read') = Determina que esse Método terá apenas Scopo de WRITE
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_USUARIO') and #oauth2.hasScope('write')")
	public ResponseEntity<UsuarioDTO> cadastrar(@RequestBody UsuarioDTO usuarioDTO, HttpServletResponse response) {
		
		UsuarioDTO usuarioSalvo = usuarioService.cadastrar(usuarioDTO);
		
		// Dispara o Evento e ele Adicionar o Header Location
		// This = Evento que Gerou o Evento
		publisher.publishEvent(new RecursoCriadoEvent(this, response, usuarioSalvo.getCodigo()));
		
		// Retornando o ResponseEntity.status(HttpStatus.CREATED) já Define o (201 Created)
		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioSalvo);
	}
	
	// DELETE (Verbo) = Anotação de Método/Função para EXCLUIR Usuario
	// Anotação @ResponseStatus Informa que Conseguiu fazer o que foi Solicitado, porém não tem nada de Conteúdo para Retornar
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_EXCLUIR_USUARIO') and #oauth2.hasScope('write')")
	public void excluir(@PathVariable Long codigo) {
		usuarioRepository.delete(codigo);
	}
	
	// PUT (Verbo) = Anotação de Método/Função para ATUALIZAR Usuario
	@PutMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_USUARIO') and #oauth2.hasScope('write')")
	public ResponseEntity<UsuarioDTO> alterar(@PathVariable Long codigo, @Valid @RequestBody UsuarioDTO usuario) {
		UsuarioDTO usuarioSalvo = usuarioService.alterar(codigo, usuario);
		usuarioSalvo.setSenha(StringUtils.EMPTY);
		return ResponseEntity.ok(usuarioSalvo);
	}
	
	// TODO EXEMPLO: Atualização Parcial de uma Propriedade
	// PUT (Verbo) = Anotação de Método/Função para ATUALIZAR Usuario
	// Passa o Parâmetro código do Objeto e a Propriedade que Deseja Alterar
	@PutMapping("/{codigo}/status")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_ALTERAR_USUARIO') and #oauth2.hasScope('write')")
	public void alterarStatus(@PathVariable Long codigo, @RequestBody Boolean ativo) {
		usuarioService.alterarStatus(codigo, ativo);
	}
	
	// Anotação @PathVariable Significa que a Requisição está Esperando dentro do {} um Valor
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_USUARIO') and #oauth2.hasScope('read')")
	public ResponseEntity<UsuarioDTO> pesquisarUsuarioCodigo(@PathVariable Long codigo) {
		UsuarioDTO usuarioSalvo = usuarioService.pesquisarUsuarioCodigo(codigo);
		return usuarioSalvo != null ? ResponseEntity.ok(usuarioSalvo) : ResponseEntity.notFound().build();
	}
	
	/*
	 * EXCEÇÕES PERSONALIZADA PARA ESSA CLASSE
	 * */
	
	// Implementa a Exceção Individual que esse Objeto vai Tratar (Ou Seja, a Exceção não é Genérica)
	@ExceptionHandler({ PerfilInexistenteInativoException.class })
	public ResponseEntity<Object> handlePerfilInexistenteInativoException(PerfilInexistenteInativoException ex) {
		String mensagemUsuario = messageSource.getMessage("perfil.inexistente-inativo", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}
	
	@ExceptionHandler({ InativarUsuarioLogadoException.class })
	public ResponseEntity<Object> handleInativarUsuarioLogadoException(InativarUsuarioLogadoException ex) {
		String mensagemUsuario = messageSource.getMessage("usuario.inativar-logado", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}

}
