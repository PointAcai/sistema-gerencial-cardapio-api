package br.com.cmmsoft.admin.cardapio.api.model.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class BooleanAtivoInativoConverter implements AttributeConverter<Boolean, String> {

    @Override
    public String convertToDatabaseColumn(Boolean value) {
        return value ? "Ativo": "Inativo";
    }

    @Override
    public Boolean convertToEntityAttribute(String value) {
        if ("Ativo".equals(value)) {
            return true;
        } else if ("Inativo".equals(value)) {
            return false;
        }

        return null;
    }

}