/*
 * Classe de Configuração do Usuário Logado
 * Utiliza o Objeto de Usuario existente no Banco de Dados
 * */
package br.com.cmmsoft.admin.cardapio.api.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import br.com.cmmsoft.admin.cardapio.api.model.Usuario;

public class UsuarioSistema extends User {

	private static final long serialVersionUID = 1L;
	
	private Usuario usuario;
	
	// Passa o Nome do Usuário para a Aplicação
	public UsuarioSistema(Usuario usuario, Collection<? extends GrantedAuthority> authorities) {
		super(usuario.getNome().concat(" - ").concat(usuario.getPerfil().getDescricao()), usuario.getSenha(), authorities);
		this.usuario = usuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

}
