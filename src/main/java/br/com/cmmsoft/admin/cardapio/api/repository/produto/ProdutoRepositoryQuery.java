package br.com.cmmsoft.admin.cardapio.api.repository.produto;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.cmmsoft.admin.cardapio.api.repository.filter.ProdutoFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoProduto;
import br.com.cmmsoft.admin.cardapio.api.service.dto.ProdutoDTO;

public interface ProdutoRepositoryQuery {
	
	public Page<ProdutoDTO> filtrar(ProdutoFilter produtoFilter, Pageable pageable);
	
	public List<ResumoProduto> resumir();
	
	public Page<ResumoProduto> pesquisarOfertas(String isOferta);
}
