package br.com.cmmsoft.admin.cardapio.api.repository.oferta;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.cmmsoft.admin.cardapio.api.model.Oferta;
import br.com.cmmsoft.admin.cardapio.api.repository.filter.OfertaFilter;
import br.com.cmmsoft.admin.cardapio.api.repository.projection.ResumoOferta;
import br.com.cmmsoft.admin.cardapio.api.service.dto.OfertaDTO;

public interface OfertaRepositoryQuery {
	
	public Page<OfertaDTO> filtrar(OfertaFilter ofertaFilter, Pageable pageable);
	
	public List<ResumoOferta> resumir();

	public List<Oferta> pesquisarOfertasAtivas();
	
}
