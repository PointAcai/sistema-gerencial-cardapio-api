/*
 * Classe Responsável pela Configuração de Login do Usuário na Aplicação
 * */
package br.com.cmmsoft.admin.cardapio.api.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.cmmsoft.admin.cardapio.api.model.Usuario;
import br.com.cmmsoft.admin.cardapio.api.repository.UsuarioRepository;

@Service
public class AppUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		Optional<Usuario> usuarioOptional = usuarioRepository.findByLogin(login);
		Usuario usuario = usuarioOptional.orElseThrow(() -> new UsernameNotFoundException("Login e/ou Senha Incorretos!"));
		/*
		 * Ao Invés de Retornar o User User(login, usuario.getSenha(), getPermissoes(usuario)), 
		 * É retornado uma Classe Configurada para o Usuário do Sistema Extendendo o User
		 * Passa para o Construtor da Classe UsuarioSistema, o Usuário do Banco de Dados e suas Permissões
		 * */
		return new UsuarioSistema(usuario, getPermissoes(usuario));
	}

	private Collection<? extends GrantedAuthority> getPermissoes(Usuario usuario) {
		// Lista de Permissões do Usuário
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		// Para Cada Permissão, é Adicionado dentro da authorities a Permissão desse Perfil Associado
		usuario.getPerfil().getPermissoes().forEach(p -> authorities.add(new SimpleGrantedAuthority(p.getPermissao().toUpperCase())));
		return authorities;
	}
	
}
