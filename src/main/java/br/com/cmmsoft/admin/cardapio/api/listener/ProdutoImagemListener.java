package br.com.cmmsoft.admin.cardapio.api.listener;

import javax.persistence.PostLoad;

import org.springframework.util.StringUtils;

import br.com.cmmsoft.admin.cardapio.api.CMMAdminCardapioApiApplication;
import br.com.cmmsoft.admin.cardapio.api.model.Produto;
import br.com.cmmsoft.admin.cardapio.api.storage.S3;

public class ProdutoImagemListener {
	
	@PostLoad
	public void postLoad(Produto produto) {
		if (StringUtils.hasText(produto.getImagem())) {
			S3 s3 = CMMAdminCardapioApiApplication.getBean(S3.class);
			produto.setUrlImagem(s3.configurarUrl(produto.getImagem()));
		}
	}
	
}
