-- tabela de usuário
create table t_parametro (
	codigo bigint(20) primary key auto_increment,
	descricao varchar(150) not null,
	chave varchar(20) not null,
	valor varchar(150) not null,
	tipo varchar(20) not null,
	status varchar(7) not null default 'Ativo'
	
) engine=innodb default charset=utf8;

-- cadastro do usuário admin (senha admin)
insert into t_parametro (descricao, chave, valor, tipo) values (
	'Logotipo do Login', 
	'LOGO_LOGIN',
	'URL DO S3',
	'Imagem'
);

insert into t_parametro (descricao, chave, valor, tipo) values (
	'Logotipo do Cardápio', 
	'LOGO_CARDAPIO',
	'URL DO S3',
	'Imagem'
);