-- tabela de categoria
create table t_categoria (
	codigo bigint(20) primary key auto_increment,
	numero_identificador bigint(20) not null,
    descricao varchar(50) not null,
    status varchar(7) not null default 'Ativo'
) engine=innodb default charset=utf8;