-- tabela de perfil
create table t_perfil (
	codigo bigint(20) primary key auto_increment,
	descricao varchar(50) not null,
	status varchar(7) not null default 'Ativo'
) engine=innodb default charset=utf8;

-- cadastro do perfil
insert into t_perfil (codigo, descricao) values (1, 'Super Admin');

insert into t_perfil (codigo, descricao) values (2, 'Admin');