-- tabela de oferta
create table t_oferta (
	codigo bigint(20) primary key auto_increment,
	valor decimal(10,2) not null,
	descricao varchar(120),
	data_validade_inicio datetime not null,
	data_validade_fim datetime,
	codigo_produto bigint(20) not null,
	status varchar(7) not null default 'Ativo',
	
	foreign key (codigo_produto) references t_produto(codigo)
) engine=innodb default charset=utf8;
