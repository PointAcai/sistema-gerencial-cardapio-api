-- tabela de produto
create table t_produto (
	codigo bigint(20) primary key auto_increment,
	numero_identificador bigint(20) not null,
	descricao varchar(50) not null,
	quantidade_medida bigint(20),
	unidade_medida_codigo bigint(20),
	categoria_codigo bigint(20) not null,
    valor decimal(10,2) not null,
    exibir_cardapio varchar(3) not null default 'Não',
    possui_oferta varchar(3) not null default 'Não',
    imagem varchar(200),
	status varchar(7) not null default 'Ativo',
	
	foreign key (unidade_medida_codigo) references t_unidade_medida (codigo),
	foreign key (categoria_codigo) references t_categoria (codigo)
) engine=innodb default charset=utf8;

-- tabela de ingrediente
create table t_ingrediente (
	codigo bigint(20) primary key auto_increment,
    descricao varchar(50) not null,
    status varchar(7) not null default 'Ativo'
) engine=innodb default charset=utf8;

-- tabela de produto e ingrediente
create table t_produto_ingrediente (
	codigo_produto bigint(20) not null,
	codigo_ingrediente bigint(20) not null,
	primary key (codigo_produto, codigo_ingrediente),
	foreign key (codigo_produto) references t_produto(codigo),
	foreign key (codigo_ingrediente) references t_ingrediente(codigo)
) engine=innodb default charset=utf8;

