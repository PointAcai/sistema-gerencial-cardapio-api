-- tabela de senha
create table t_controle_pedido (
	codigo bigint(20) primary key auto_increment,
	senha bigint(20) not null,
    data datetime not null,
    nome varchar(50),
    status varchar(7) not null default 'Ativo'
) engine=innodb default charset=utf8;