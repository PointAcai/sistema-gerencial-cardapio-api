-- tabela de unidade de medida
create table t_unidade_medida (
	codigo bigint(20) primary key auto_increment,
	sigla varchar(10) not null,
    descricao varchar(50) not null,
    status varchar(7) not null default 'Ativo'
) engine=innodb default charset=utf8;