-- tabela de permissão
create table t_permissao (
	codigo bigint(20) primary key,
	descricao varchar(50) not null,
	permissao varchar(50) not null
) engine=innodb default charset=utf8;

-- tabela de perfil e permissão
create table t_perfil_permissao (
	codigo_perfil bigint(20) not null,
	codigo_permissao bigint(20) not null,
	primary key (codigo_perfil, codigo_permissao),
	foreign key (codigo_perfil) references t_perfil(codigo),
	foreign key (codigo_permissao) references t_permissao(codigo)
) engine=innodb default charset=utf8;

-- Permissão de Acesso a Funcionalidade de Usuário
INSERT INTO t_permissao (codigo, descricao, permissao) values (1, 'USUÁRIO - Cadastrar', 'ROLE_CADASTRAR_USUARIO');
INSERT INTO t_permissao (codigo, descricao, permissao) values (2, 'USUÁRIO - Alterar', 'ROLE_ALTERAR_USUARIO');
INSERT INTO t_permissao (codigo, descricao, permissao) values (3, 'USUÁRIO - Excluir', 'ROLE_EXCLUIR_USUARIO');
INSERT INTO t_permissao (codigo, descricao, permissao) values (4, 'USUÁRIO - Pesquisar', 'ROLE_PESQUISAR_USUARIO');

-- Permissão de Acesso a Funcionalidade de Perfil
INSERT INTO t_permissao (codigo, descricao, permissao) values (5, 'PERFIL - Cadastrar', 'ROLE_CADASTRAR_PERFIL');
INSERT INTO t_permissao (codigo, descricao, permissao) values (6, 'PERFIL - Alterar', 'ROLE_ALTERAR_PERFIL');
INSERT INTO t_permissao (codigo, descricao, permissao) values (7, 'PERFIL - Excluir', 'ROLE_EXCLUIR_PERFIL');
INSERT INTO t_permissao (codigo, descricao, permissao) values (8, 'PERFIL - Pesquisar', 'ROLE_PESQUISAR_PERFIL');

-- Permissão de Acesso a Funcionalidade de Produto
INSERT INTO t_permissao (codigo, descricao, permissao) values (9, 'PRODUTO - Cadastrar', 'ROLE_CADASTRAR_PRODUTO');
INSERT INTO t_permissao (codigo, descricao, permissao) values (10, 'PRODUTO - Alterar', 'ROLE_ALTERAR_PRODUTO');
INSERT INTO t_permissao (codigo, descricao, permissao) values (11, 'PRODUTO - Excluir', 'ROLE_EXCLUIR_PRODUTO');
INSERT INTO t_permissao (codigo, descricao, permissao) values (12, 'PRODUTO - Pesquisar', 'ROLE_PESQUISAR_PRODUTO');

-- Permissão de Acesso a Funcionalidade de Ingrediente
INSERT INTO t_permissao (codigo, descricao, permissao) values (13, 'INGREDIENTE - Cadastrar', 'ROLE_CADASTRAR_INGREDIENTE');
INSERT INTO t_permissao (codigo, descricao, permissao) values (14, 'INGREDIENTE - Alterar', 'ROLE_ALTERAR_INGREDIENTE');
INSERT INTO t_permissao (codigo, descricao, permissao) values (15, 'INGREDIENTE - Excluir', 'ROLE_EXCLUIR_INGREDIENTE');
INSERT INTO t_permissao (codigo, descricao, permissao) values (16, 'INGREDIENTE - Pesquisar', 'ROLE_PESQUISAR_INGREDIENTE');

-- Permissão de Acesso a Funcionalidade de Unidade Medida
INSERT INTO t_permissao (codigo, descricao, permissao) values (17, 'UNIDADE MEDIDA - Cadastrar', 'ROLE_CADASTRAR_UNIDADE_MEDIDA');
INSERT INTO t_permissao (codigo, descricao, permissao) values (18, 'UNIDADE MEDIDA - Alterar', 'ROLE_ALTERAR_UNIDADE_MEDIDA');
INSERT INTO t_permissao (codigo, descricao, permissao) values (19, 'UNIDADE MEDIDA - Excluir', 'ROLE_EXCLUIR_UNIDADE_MEDIDA');
INSERT INTO t_permissao (codigo, descricao, permissao) values (20, 'UNIDADE MEDIDA - Pesquisar', 'ROLE_PESQUISAR_UNIDADE_MEDIDA');

-- Permissão de Acesso a Funcionalidade de Categoria
INSERT INTO t_permissao (codigo, descricao, permissao) values (21, 'CATEGORIA - Cadastrar', 'ROLE_CADASTRAR_CATEGORIA');
INSERT INTO t_permissao (codigo, descricao, permissao) values (22, 'CATEGORIA - Alterar', 'ROLE_ALTERAR_CATEGORIA');
INSERT INTO t_permissao (codigo, descricao, permissao) values (23, 'CATEGORIA - Excluir', 'ROLE_EXCLUIR_CATEGORIA');
INSERT INTO t_permissao (codigo, descricao, permissao) values (24, 'CATEGORIA - Pesquisar', 'ROLE_PESQUISAR_CATEGORIA');

-- Permissão de Acesso a Funcionalidade de Oferta
INSERT INTO t_permissao (codigo, descricao, permissao) values (25, 'OFERTA - Cadastrar', 'ROLE_CADASTRAR_OFERTA');
INSERT INTO t_permissao (codigo, descricao, permissao) values (26, 'OFERTA - Alterar', 'ROLE_ALTERAR_OFERTA');
INSERT INTO t_permissao (codigo, descricao, permissao) values (27, 'OFERTA - Excluir', 'ROLE_EXCLUIR_OFERTA');
INSERT INTO t_permissao (codigo, descricao, permissao) values (28, 'OFERTA - Pesquisar', 'ROLE_PESQUISAR_OFERTA');

-- Permissão de Acesso a Funcionalidade de Configurações
INSERT INTO t_permissao (codigo, descricao, permissao) values (29, 'OFERTA - Cadastrar', 'ROLE_CADASTRAR_PARAMETROS');
INSERT INTO t_permissao (codigo, descricao, permissao) values (30, 'OFERTA - Alterar', 'ROLE_ALTERAR_PARAMETROS');
INSERT INTO t_permissao (codigo, descricao, permissao) values (31, 'OFERTA - Excluir', 'ROLE_EXCLUIR_PARAMETROS');
INSERT INTO t_permissao (codigo, descricao, permissao) values (32, 'OFERTA - Pesquisar', 'ROLE_PESQUISAR_PARAMETROS');

-- Permissão de Acesso a Funcionalidade de Controle de Pedido
INSERT INTO t_permissao (codigo, descricao, permissao) values (33, 'OFERTA - Cadastrar', 'ROLE_CADASTRAR_CONTROLE_PEDIDOS');
INSERT INTO t_permissao (codigo, descricao, permissao) values (34, 'OFERTA - Alterar', 'ROLE_ALTERAR_CONTROLE_PEDIDOS');
INSERT INTO t_permissao (codigo, descricao, permissao) values (35, 'OFERTA - Excluir', 'ROLE_EXCLUIR_CONTROLE_PEDIDOS');
INSERT INTO t_permissao (codigo, descricao, permissao) values (36, 'OFERTA - Pesquisar', 'ROLE_PESQUISAR_CONTROLE_PEDIDOS');

-- Concede a Permissão de Acesso ao Usuário ADMIN à Funcionalidade de Usuário
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 1);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 2);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 3);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 4);

-- Concede a Permissão de Acesso ao Usuário ADMIN à Funcionalidade de Perfil
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 5);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 6);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 7);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 8);

-- Concede a Permissão de Acesso ao Usuário ADMIN à Funcionalidade de Produto
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 9);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 10);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 11);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 12);

-- Concede a Permissão de Acesso ao Usuário ADMIN à Funcionalidade de Ingrediente
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 13);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 14);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 15);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 16);

-- Concede a Permissão de Acesso ao Usuário ADMIN à Funcionalidade de Unidade de Medida
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 17);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 18);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 19);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 20);

-- Concede a Permissão de Acesso ao Usuário ADMIN à Funcionalidade de Categoria
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 21);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 22);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 23);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 24);

-- Concede a Permissão de Acesso ao Usuário ADMIN à Funcionalidade de Oferta
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 25);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 26);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 27);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 28);

-- Concede a Permissão de Acesso ao Usuário ADMIN à Funcionalidade de Configurações
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 29);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 30);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 31);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 32);

-- Concede a Permissão de Acesso ao Usuário ADMIN à Funcionalidade de Controle de Pedidos
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 33);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 34);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 35);
INSERT INTO t_perfil_permissao (codigo_perfil, codigo_permissao) VALUES (1, 36);