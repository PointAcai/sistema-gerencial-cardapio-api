-- tabela de usuário
create table t_usuario (
	codigo bigint(20) primary key auto_increment,
	nome varchar(50) not null,
	login varchar(20) not null,
	senha varchar(150) not null,
	status varchar(7) not null default 'Ativo',
	codigo_perfil bigint(20) not null,
	
	foreign key (codigo_perfil) references t_perfil(codigo)
) engine=innodb default charset=utf8;

-- cadastro do usuário admin (senha admin)
insert into t_usuario (nome, login, senha, codigo_perfil) values (
	'Administrador', 
	'ADM_CMM',
	'$2a$10$R5DQvseYHeCl16TMJrcqcOD.c6iI8Np4xxuTqSsgLILtdWuA/747W',
	1
);